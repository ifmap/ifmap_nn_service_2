package aaplication;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.tuple.Pair;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;

import neural_network.ModelJob;
import neural_network.ModelUtils;
import utilities.FileUtils;

public class IFMAP_DataCollection {
	
	public static File train = FileUtils.loadFileOnDesktop("Data\\Fresh-Rotten\\train");
	public static File test = FileUtils.loadFileOnDesktop("Data\\Fresh-Rotten\\test");
	public static final File EVALUTE_FOLDER = FileUtils.loadFileOnDesktop("Data\\Networks\\Eval");
	private static final int RECOG_LABELS = 3;
	private static MultiLayerNetwork recognitionModel;
	private static MultiLayerNetwork[] evaluationModels;
	private static MultiLayerNetwork model;
	
	public static void main(String[] args) throws IOException {
		loadRecogModel();
		loadEvalModels();
//		model = MultiLayerNetwork.load(FileUtils.loadFileOnDesktop("Data\\Eval-CNN-All_6_9681.zip"), false);
		BufferedImage img = null;
		long startTime = System.currentTimeMillis();
		int good = 0;
		int bad = 0;
		
		for(File c : train.listFiles()) {
			String accurate = c.getName().substring(0, c.getName().indexOf('-'));
			for(File i : c.listFiles()) {
				try {
					img = ImageIO.read(i);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String prediction = "";
				
				if(img != null) {
					prediction = evaluateFruitMaturity(img);
				}
				System.out.println("P:" + prediction + " E:" + accurate);
				if(prediction.equalsIgnoreCase(accurate)) 
					good++;
				 else
					bad++;
				
			}
		}
		System.out.println("Good: " + good + " Bad: " + bad + " Time: " + (System.currentTimeMillis() - startTime) / 1000);
		System.out.println("Accuracy: " + good / (good + bad));
	}
	
	private static boolean loadRecogModel() {
		try {
			recognitionModel = ModelUtils.loadModel(ModelJob.RECOGNIZE, "", RECOG_LABELS, true, false);
			recognitionModel.init();
			return true;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	private static boolean loadEvalModels() {
		evaluationModels = new MultiLayerNetwork[EVALUTE_FOLDER.listFiles().length];
		try {
			int i = 0;
			for(File f : EVALUTE_FOLDER.listFiles()) {
				evaluationModels[i] = MultiLayerNetwork.load(f, false);
				i++;
			}
			return true;
		} catch (NullPointerException e) {
			System.out.println("Model is nonexistant");
			return false;
		} catch (IOException e) {
			System.out.println("Cannot reach file.");
			e.printStackTrace();
			return false;
		}
	}
	
	private static int recognizeFruit(BufferedImage img) {
		if (img == null) {
			System.out.println("Img is null");
			return -1;
		}
		
		Pair<Integer, Double> resultPair = result(img, recognitionModel);
		
		return resultPair.getLeft();
	}

	private static String evaluateFruitMaturity(BufferedImage img) {
		String fresh = "";
		if(img == null) {
			System.err.println("Img is null");
			return "";
		}
	
		//COMMENT THESE LINES FOR SINGLE
		int recogResult = recognizeFruit(img);
		model = evaluationModels[recogResult];
		
		Pair<Integer, Double> resultPair = result(img, model);
		
		fresh = getResultFromId(resultPair.getLeft());

		return fresh;
	}
	
	private static Pair<Integer, Double> result(BufferedImage img, MultiLayerNetwork model) {
		Pair<Integer, Double> resultPair;
		INDArray output = ModelUtils.passImage(img, model);
//		System.out.println("Output: " + output);
		
		if(output == null) {
			resultPair = Pair.of(-1, 0.0);
		} else {
			resultPair = getResultPair(output, model);
		}
		
		return resultPair;
	}
	
	private static Pair<Integer, Double> getResultPair(INDArray output, MultiLayerNetwork model) {
		Pair<Integer, Double> result = getResultId(output.getRow(0));
		return result;
	}

	private static Pair<Integer, Double> getResultId(INDArray outputs) {
		Pair<Integer, Double> result;
		int id = 0;
		double value = outputs.getDouble(id);

		for (int i = 0; i < outputs.length(); i++) {
			if (outputs.getDouble(i) > value) {
				id = i;
				value = outputs.getDouble(i);
			}
		}
		result = Pair.of(id, value);
		return result;
	}
	
	private static String getResultFromId(int id) {
		return getEvaluationFromId(id);			
	}
	
	private static String getEvaluationFromId(int id) {
		switch (id) {
		case (0):
			return "Fresh";
		case (1):
			return "Rotten";
		default:
			return "";
		}
	}
}
