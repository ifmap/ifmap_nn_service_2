package aaplication;

import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.tuple.Pair;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;

import neural_network.ModelJob;
import neural_network.ModelUtils;
import utilities.FileUtils;
import utilities.Utils;

public class IFMAP_Service {
	
	private static final double TRESHOLD_FRUIT_ACCURACY = 0.90;
	private static final double TRESHOLD_FRESH_ACCURACY = 0.70;
	private static final String NOT_RECOGNIZED = "None";
	
	private static final float RESCALE_FACT = 1.5f;
	private static final int RESCALE_OFFSET = 30;
	
	public static final String backendPublic = "C:\\wamp64\\backend\\public\\";
	private static final String JSON_PATH = "";
	private static final File DB_FILE = utilities.FileUtils.loadFileFromBackend("service_db\\aliments\\");
	public static final String EVALUTE_PATH_START = "evaluate";
	private static MultiLayerNetwork recognitionModel;
	private static MultiLayerNetwork evaluationModel;
	private long currentID = 1;
	
	static final String TEST_PICTURE = "Data\\Fresh-Rotten\\orange.png";
	public String answer = "";
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		File imgFile = FileUtils.loadFileFromBackend(TEST_PICTURE);
		
		if(args.length != 0 && args[0] != null) {
			System.out.println(args[0]);
			imgFile = new File(args[0]);
		}
		
		BufferedImage img = null;
		
		if(imgFile == null) {
			System.out.println("Couldn't load img file");
			return;
		}
		
		try {
			img = ImageIO.read(imgFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.print(new IFMAP_Service().evaluateFruitMaturity(img));
	}
	
	private boolean loadRecogModel() {
		try {
			int recogLabels = DB_FILE.listFiles().length;
			recognitionModel = ModelUtils.loadModel(ModelJob.RECOGNIZE, "", recogLabels, true, true);
			recognitionModel.init();
			return true;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean loadEvalModel(String fruitName) {
		try {
			int nbOfEvals = FileUtils.getFileInChildren(DB_FILE, fruitName).listFiles().length;
			evaluationModel = ModelUtils.loadModel(ModelJob.EVALUATE, fruitName, nbOfEvals, true, true);
			return true;
		} catch (NullPointerException e) {
			System.out.println("Model is nonexistant");
			return false;
		} catch (IOException e) {
			System.out.println("Cannot reach file.");
			e.printStackTrace();
			return false;
		}
	}
	
	private Pair<String, Double> recognizeFruit(BufferedImage img) {
//		System.out.println("Recognizing fruit...");
		String fruit = NOT_RECOGNIZED;
		double resultAccuracy = 0;
		if (img == null) {
			System.out.println("Img is null");
			return Pair.of(fruit, resultAccuracy);
		}
		
		if(!loadRecogModel()) {
			System.out.println("Recog model is null");
			return Pair.of(fruit, resultAccuracy);
		}
		
		Pair<Integer, Double> resultPair = freshnessResult(img, recognitionModel);
		resultAccuracy = resultPair.getRight();
		
		if(isPreciseEnough(resultAccuracy, TRESHOLD_FRUIT_ACCURACY)) { //If the recognition prediction is accurate enough
			fruit = getResultFromId("", resultPair.getLeft(), ModelJob.RECOGNIZE);
		}
		
		return Pair.of(fruit, resultAccuracy);
	}

	private String evaluateFruitMaturity(BufferedImage img) {
		String result = "";
		String aliment = NOT_RECOGNIZED;
		String fresh = NOT_RECOGNIZED;
		double freshAcc = 0;
		if(img == null) {
			System.err.println("Img is null");
			return convertToJSON(NOT_RECOGNIZED, NOT_RECOGNIZED, 0, 0, "");
		}
	
		Pair<String, Double> recogResult = recognizeFruit(img);
		aliment = recogResult.getLeft();
		
		if(!aliment.equalsIgnoreCase(NOT_RECOGNIZED)) { //If the fruit was recognized properly
			
			if(!loadEvalModel(aliment.toLowerCase()))
				return convertToJSON(NOT_RECOGNIZED, NOT_RECOGNIZED, 0, 0, "");
			
			img = adjustColor(img); //Changes image brightness and contrast for better freshness evaluation
			
			Pair<Integer, Double> freshPair = freshnessResult(img, evaluationModel);
			
			freshAcc = freshPair.getRight();
			
			if(isPreciseEnough(freshAcc, TRESHOLD_FRESH_ACCURACY)) //If the freshness prediction is accurate enough
				fresh = getResultFromId(aliment, freshPair.getLeft(), ModelJob.EVALUATE);
		}

		result = convertToJSON(aliment, fresh, recogResult.getRight(), freshAcc, "");
		return result;
	}
	
	private Pair<Integer, Double> freshnessResult(BufferedImage img, MultiLayerNetwork model) {
		Pair<Integer, Double> resultPair;
		INDArray output = ModelUtils.passImage(img, model);
//		System.out.println("Output: " + output);
		
		if(output == null) {
			resultPair = Pair.of(-1, 0.0);
		} else {
			resultPair = getResultPair(output, model);
		}
		
		return resultPair;
	}
	
	private Pair<Integer, Double> getResultPair(INDArray output, MultiLayerNetwork model) {
		Pair<Integer, Double> result = getResultId(output.getRow(0));
		return result;
	}

	private Pair<Integer, Double> getResultId(INDArray outputs) {
		Pair<Integer, Double> result;
		int id = 0;
		double value = outputs.getDouble(id);

		for (int i = 0; i < outputs.length(); i++) {
			if (outputs.getDouble(i) > value) {
				id = i;
				value = outputs.getDouble(i);
			}
		}
		result = Pair.of(id, value);
		return result;
	}
	
	private String getResultFromId(String aliment, int id, ModelJob job) {
		if(job.equals(ModelJob.EVALUATE))
			return getEvaluationFromId(aliment, id);
		else
			return getFruitInFilesFromId(id);
	}
	
	// TODO: Read from file in zip
	private String getFruitInFilesFromId(int id) {
//		System.out.println("Recog ID: " + id);
		File labelsFile = DB_FILE;
		return labelsFile.listFiles()[id].getName();
	}
	
	private String getEvaluationFromId(String aliment, int id) {
		File alimentFile = FileUtils.getFileInChildren(DB_FILE, aliment);
		return alimentFile.listFiles()[id].getName();
	}
	
	private boolean isPreciseEnough(double acc, double treshold) {
		return acc >= treshold;
	}
	
	private String convertToJSON(String name, String result, double fruitAcc, double freshAcc, String extraData) {
		String[][] data = new String[5][2];
		
		data[0][0] = "Name";
		data[1][0] = "Result";
		data[2][0] = "CertaintyFruit";
		data[3][0] = "CertaintyFresh";
		data[4][0] = "ExtraData";
		
		data[0][1] = name;
		data[1][1] = result;
		data[2][1] = Utils.round(fruitAcc * 100, 2) + "";
		data[3][1] = Utils.round(freshAcc * 100, 2) + "";
		data[4][1] = extraData;
		
		return utilities.FileUtils.writeResultsToJSON(data, JSON_PATH + currentID + ".json");
	}
	
	private BufferedImage adjustColor(BufferedImage img) {
		BufferedImage adjustedImage = null;
		RescaleOp rescaleOp = new RescaleOp(RESCALE_FACT, RESCALE_OFFSET, null); //Change image contrast and brightness
		adjustedImage = rescaleOp.filter(img, adjustedImage);  // Source and destination are the same.
		
		FileUtils.exportPicture(adjustedImage, "jpg", FileUtils.loadFileFromBackend("Data\\ColorAdjust\\bright" + RESCALE_FACT + "-" + RESCALE_OFFSET + ".jpg"));
		return adjustedImage;
	}
}
