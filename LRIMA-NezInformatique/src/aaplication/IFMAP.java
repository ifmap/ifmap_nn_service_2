package aaplication;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.poi.hpsf.Thumbnail;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import com.sun.jna.platform.FileUtils;

import Drawable.ImageViewer;
import neural_network.ModelJob;
import neural_network.ModelUtils;
import neural_network.MultiLayerNetworkParams;
import oshi.util.FileUtil;
import pictureUtils.PictureReader;
import utilities.JUtilities;
import utilities.*;

import javax.swing.SpringLayout;
import javax.swing.JMenuBar;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;

public class IFMAP extends JFrame {
	
	private static final String JSON_PATH = "resultsSaved\\JSON\\";
	private static final File LABEL_FILE = utilities.FileUtils.loadFileOnDesktop("Data\\Fresh-Rotten\\train-recog");
	private static final File DEFAULT_IMAGECHOOSER_PATH = utilities.FileUtils.loadFileOnDesktop("Data");
	private static final String TRAINED_PATH = "Trained Networks\\";
	public static final String EVALUTE_PATH_START = "evaluate";
	private static final int RECOG_LABELS = 3;
	private static final int EVAL_LABELS = 2;
	private static MultiLayerNetwork recognitionModel;
	private static MultiLayerNetwork evaluationModel;
	private long currentID = 1;
	private final JFileChooser fileChooser;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JButton btnChoosePicture;
	private JLabel lblFileChosen;
	private JButton btnEvaluateAge;
	private JLabel lblResult;

	private BufferedImage imageToEvaluate = null;
	private ImageViewer imageViewer;
	private JButton btnRecognizeFruit;
	private JSeparator separator;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IFMAP frame = new IFMAP();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IFMAP() {

		fileChooser = new JFileChooser(DEFAULT_IMAGECHOOSER_PATH);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		btnChoosePicture = new JButton("Choisir une image");
		btnChoosePicture.setBounds(29, 31, 166, 29);
		btnChoosePicture.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				imageToEvaluate = choosePicture();
			}
		});
		contentPane.setLayout(null);
		btnChoosePicture.setFont(new Font("Verdana", Font.PLAIN, 11));
		contentPane.add(btnChoosePicture);

		lblFileChosen = new JLabel("Fichier choisi:");
		lblFileChosen.setBounds(29, 82, 267, 15);
		lblFileChosen.setFont(new Font("Verdana", Font.PLAIN, 11));
		contentPane.add(lblFileChosen);

		btnEvaluateAge = new JButton("\u00C9valuer son \u00E2ge");
		btnEvaluateAge.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//				System.out.println("ImgToEv:" + imageToEvaluate);
				evaluateFruitMaturity(imageToEvaluate);
			}
		});
		btnEvaluateAge.setBounds(264, 160, 144, 37);
		btnEvaluateAge.setFont(new Font("Verdana", Font.PLAIN, 11));
		contentPane.add(btnEvaluateAge);

		lblResult = new JLabel("R\u00E9sultat:");
		lblResult.setHorizontalAlignment(SwingConstants.CENTER);
		lblResult.setBounds(29, 208, 380, 20);
		lblResult.setFont(new Font("Verdana", Font.BOLD, 15));
		contentPane.add(lblResult);

		imageViewer = new ImageViewer();
		imageViewer.setBounds(283, 20, 100, 100);
		imageViewer.initialize(null, imageViewer.getBounds());
		contentPane.add(imageViewer);

		btnRecognizeFruit = new JButton("Reconna\u00EEtre le fruit");
		btnRecognizeFruit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				recognizeFruit(imageToEvaluate);
			}
		});
		btnRecognizeFruit.setBounds(29, 160, 144, 37);
		contentPane.add(btnRecognizeFruit);
		
		separator = new JSeparator();
		separator.setBounds(29, 141, 380, 8);
		contentPane.add(separator);
	}
	
	private BufferedImage choosePicture() {
		Pair<BufferedImage, String> pair = JUtilities.choosePicture(fileChooser);
		if(pair == null)
			return null;
		
		imageViewer.setImage(pair.getLeft());
		lblFileChosen.setText("Fichier choisi: " + pair.getRight());
		return pair.getLeft();
	}
	
	private boolean loadRecogModel() {
		try {
			recognitionModel = ModelUtils.loadModel(ModelJob.RECOGNIZE, "", RECOG_LABELS, true, false);
			recognitionModel.init();
			return true;
		} catch (NullPointerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	private String recognizeFruit(BufferedImage img) {
		System.out.println("Recognizing fruit...");
		if (img == null) {
			System.out.println("Img is null");
			return "";
		}
		
		if(!loadRecogModel()) {
			System.out.println("Recog model is null");
			return "";
		}
		
		String result = "";
		Pair<Integer, Double> resultPair = result(img, recognitionModel);
		double resultAccuracy = resultPair.getRight();
		result = getResultFromId(resultPair.getLeft(), ModelJob.RECOGNIZE);
		showResult(result, resultAccuracy);
		return result;
	}

	private String evaluateFruitMaturity(BufferedImage img) {
		System.out.println("Evaluating fruit...");
		if(img == null) {
			System.err.println("Img is null");
			return "";
		}
		
		String fruit = recognizeFruit(img);
		System.out.println("Fruit: " + fruit);
		System.out.println("Loading fruit maturity evaluation model");
		try {
			evaluationModel = ModelUtils.loadModel(ModelJob.EVALUATE, fruit.toLowerCase(), EVAL_LABELS, true, false);
		} catch (NullPointerException e) {
			System.out.println("Eval model is nonexistant");
			return "";
		} catch (IOException e) {
			System.out.println("Cannot reach file.");
			e.printStackTrace();
			return "";
		}
		
		String result = "";
		Pair<Integer, Double> resultPair = result(img, evaluationModel);
		double resultAccuracy = resultPair.getRight();
		result = getResultFromId(resultPair.getLeft(), ModelJob.EVALUATE);
		showResult(result, resultAccuracy);
		
		convertToJSON(fruit, result, resultAccuracy);
		
		return result;
	}
	
	private Pair<Integer, Double> result(BufferedImage img, MultiLayerNetwork model) {
		Pair<Integer, Double> resultPair;
		INDArray output = ModelUtils.passImage(img, model);
		System.out.println("Output: " + output);
		
		if(output == null) {
			resultPair = Pair.of(-1, 0.0);
		} else {
			resultPair = getResultPair(output, model);
		}
		
		return resultPair;
	}
	
	private Pair<Integer, Double> getResultPair(INDArray output, MultiLayerNetwork model) {
		Pair<Integer, Double> result = getResultId(output.getRow(0));
		return result;
	}

	private Pair<Integer, Double> getResultId(INDArray outputs) {
		Pair<Integer, Double> result;
		int id = 0;
		double value = outputs.getDouble(id);

		for (int i = 0; i < outputs.length(); i++) {
			if (outputs.getDouble(i) > value) {
				id = i;
				value = outputs.getDouble(i);
			}
		}
		result = Pair.of(id, value);
		return result;
	}
	
	private String getResultFromId(int id, ModelJob job) {
		if(job.equals(ModelJob.EVALUATE))
			return getEvaluationFromId(id);
		else
			return getFruitInFilesFromId(id);
	}
	
	// TODO: Read from file in zip
	private String getFruitInFilesFromId(int id) {
		System.out.println("Recog ID: " + id);
		File labelsFile = LABEL_FILE;
		return labelsFile.listFiles()[id].getName();
	}
	
	private String getEvaluationFromId(int id) {
		switch (id) {
		case (0):
			return "Fresh";
		case (1):
			return "Rotten";
		default:
			return "None";
		}
	}
	
	private void showResult(String result, double resultAccuracy) {
		String text = "R�sultat: " + result + " with " + Utils.round(resultAccuracy * 100, 2) + "% certainty";
		System.out.println(text);
		lblResult.setText(text);
	}
	
	private String convertToJSON(String name, String result, double accuracy) {
		String[][] data = new String[3][2];
		
		data[0][0] = "Name";
		data[1][0] = "Result";
		data[2][0] = "Certainty";
		
		data[0][1] = name;
		data[1][1] = result;
		data[2][1] = Utils.round(accuracy * 100, 2) + "";
		
		return utilities.FileUtils.writeResultsToJSON(data, JSON_PATH + currentID + ".json");
	}
}
