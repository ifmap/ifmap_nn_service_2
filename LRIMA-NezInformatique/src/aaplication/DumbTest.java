package aaplication;

import java.io.IOException;

import org.deeplearning4j.nn.modelimport.keras.KerasModelImport;
import org.deeplearning4j.nn.modelimport.keras.exceptions.InvalidKerasConfigurationException;
import org.deeplearning4j.nn.modelimport.keras.exceptions.UnsupportedKerasConfigurationException;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import utilities.FileUtils;

public class DumbTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String modelPath = FileUtils.loadFileOnDesktop("Data_IFMAP\\model1.h5").getPath();
		MultiLayerNetwork model = null;
		try {
			 model = KerasModelImport.importKerasSequentialModelAndWeights(modelPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKerasConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedKerasConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(model.summary());
	}

}
