package aaplication;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.JTabbedPane;

//TODO: Bigger tabs -> Custom Tab Class
public class IFMAP_2 extends JFrame {

	private JPanel contentPane;
	JMenuBar menuBar;
	JMenu mnMenu;
	JMenuItem mntmAide;
	JMenuItem mntmParametres;
	JMenuItem mntmDebug;
	JTabbedPane tabbedPane;
	JPanel panel_Train;
	JPanel panel_Test;
	private JSeparator separator;
	//
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					IFMAP_2 frame = new IFMAP_2();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public IFMAP_2() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1280, 720);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnMenu = new JMenu("Menu");
		menuBar.add(mnMenu);
		
		mntmAide = new JMenuItem("Aide");
		mnMenu.add(mntmAide);
		
		mntmParametres = new JMenuItem("Param\u00E8tres");
		mnMenu.add(mntmParametres);
		
		mntmDebug = new JMenuItem("Debug");
		mntmDebug.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				debug();
			}
		});
		mnMenu.add(mntmDebug);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		
		panel_Train = new JPanel();
		tabbedPane.addTab("TRAIN", null, panel_Train, null);
		panel_Train.setLayout(null);
		
		separator = new JSeparator();
		separator.setBounds(0, 310, 1248, 2);
		panel_Train.add(separator);
		
		panel_Test = new JPanel();
		tabbedPane.addTab("TEST", null, panel_Test, null);
	}
	
	private void debug() {
		System.out.println("ContentPane: " + contentPane.getWidth() + "/" + contentPane.getHeight());
		System.out.println("Panel Train: " + panel_Train.getWidth() + "/" + panel_Train.getHeight());
	}
}
