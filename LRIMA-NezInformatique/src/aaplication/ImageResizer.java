package aaplication;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Random;

import javax.imageio.ImageIO;

import net.coobird.thumbnailator.Thumbnails;
import utilities.FileUtils;

public class ImageResizer {
	
	private static int targetWidth = 224;
	private static int targetHeight = 224;
	private static boolean resize = false;
	
	public static void main(String[] args) {
		File folder =  FileUtils.loadFileOnDesktop("Data_IFMAP\\avocats_dataset\\Test");
		resizeImagesInFile(folder);
//		Thumbnails.of(img).size(targetWidth, targetHeight).asBufferedImage();
	}
	
	private static void resizeImagesInFile(File folder) {
		System.out.println("Doing file: " + folder.getName());
		ArrayList<BufferedImage> images = new ArrayList<BufferedImage>();
		ArrayList<File> filesOfImages = new ArrayList<File>();
		for(int i = 0; i < folder.listFiles().length; i++) {
			File file = folder.listFiles()[i];
			if(file.isDirectory()) {
				resizeImagesInFile(file);
				continue;
			}
			
			System.out.println("Doing image: " + file);
			BufferedImage img = null;
			try {
				img = ImageIO.read(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}
			
			BufferedImage resizedImage = img;
			if(resize && (img.getWidth() != targetWidth || img.getHeight() != targetHeight)) {
				try {
					resizedImage = Thumbnails.of(img).size(targetWidth, targetHeight).asBufferedImage();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					continue;
				}
			}
			images.add(resizedImage);
			filesOfImages.add(file);
		}
		for(int i = 0; i < images.size(); i++) {
			BufferedImage img = images.get(i);
			boolean exported = false;
			try {
				exported = ImageIO.write(img, "jpg", new File(folder.getPath() + "\\" + i + "_" + new Random().nextInt(10000) + "_resized.jpg"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(!exported)
				continue;
			filesOfImages.get(i).delete();
		}
		
	}
}
