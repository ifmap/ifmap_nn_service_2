package zcustom_nn;

public class Data {

	float[] data;
	
	public Data(float[] data) {
		this.data = data;
	}
	
	public float[] getData() {
		return this.data;
	}
}
