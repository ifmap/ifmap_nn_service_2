package zcustom_nn;

public enum ActivationFunction {
	Sigmoid, Relu, LeakyRelu, Softmax, Softplus, TanH
}
