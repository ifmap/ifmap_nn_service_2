package utilities;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

import org.apache.commons.lang3.tuple.Pair;

public class JUtilities {
	
	/**
	 * Used to get a picture through a JFileChooser. Returns a pair of the picture 
	 * as a BufferedImage and the picture's path
	 * @param fileChooser The JFileChooser to use
	 * @return A Pair of the pictue as a BufferedImage and the picture's path as a String
	 */
	public static Pair<BufferedImage, String> choosePicture(JFileChooser fileChooser) {
		Pair<BufferedImage, String> pair = null;
		int choice = fileChooser.showOpenDialog(null);
		System.out.println(choice);

		if (choice == JFileChooser.APPROVE_OPTION) {
			pair = setSelectedPicture(fileChooser);
		} else {
			System.out.println("Open command cancelled by user.");
		}
		return pair;
	}
	
	/**
	 * Selects the picture in the picture path. Used when JFileChooser choice is confirmed
	 * @param fileChooser The JFileChooser to use
	 * @return A Pair of the pictue as a BufferedImage and the picture's path as a String
	 */
	public static Pair<BufferedImage, String> setSelectedPicture(JFileChooser fileChooser) {
		BufferedImage img = null;
		File file = fileChooser.getSelectedFile();
		// This is where a real application would open the file.
		if (file == null)
			return null;

		System.out.println("Opening: " + file.getName() + ".");
		try {
			img = ImageIO.read(file);

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Pair<BufferedImage, String> pair;
		pair = Pair.of(img, file.getParentFile().getName() + "\\" +  file.getName());
		return pair;
	}
}
