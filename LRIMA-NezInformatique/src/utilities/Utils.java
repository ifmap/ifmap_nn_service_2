package utilities;

import java.awt.image.BufferedImage;
import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

import zcustom_nn.StatUtil;

public class Utils {
	
	public static HashMap<Integer, String> switchHashMap(HashMap<String, Integer> map){
		HashMap<Integer, String> newMap = new HashMap<Integer, String>();
		
		for(String key : map.keySet()) {
			newMap.put(map.get(key), key);
		}
		
		return newMap;
	}
	
	public static String getAndRemove(ArrayList<String> list, int id) {
		String s = list.get(id);
		list.remove(id);
		return s;
	}
	
	public static ArrayList<Double> convertStringToDouble(ArrayList<String> list, boolean doDelete){
		ArrayList<Double> doubleList = new ArrayList<Double>();
		
		for(String s : list) {
			double d = 0;
			try {
				d = Double.parseDouble(s);
				if(doDelete) {
					list.remove(s);
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			doubleList.add(d);
		}
		return doubleList;
	}
	
	public static boolean isNumeric(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
	}
	
	public static double round(double d, int decimals) {
		return Math.round(d * Math.pow(10, decimals)) / Math.pow(10, decimals);
	}
	
	public static String getFruitInFileName(String name, String delimitation) {
		int delimitationID = name.indexOf(delimitation);
		if(delimitationID != -1)
			return name.substring(0, delimitationID);
		else
			return name;
	}
	
	public static int[] getPictureSize(BufferedImage img) {
		return new int[] {img.getWidth(), img.getHeight()};
	}
	
	public static String getMachine() {
		return System.getProperty("user.name");
	}
	
	public static File getLocationToSave(String SAVE_PATH, String modelName, int nOut, double accuracy) {		
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		String fileName = StatUtil.normalizeFileName(modelName + "_" + nOut + "_" + ts + "_" + accuracy);

		return new File(SAVE_PATH + fileName + ".zip"); // Where to save the network. Note: the file is	
	}
}

