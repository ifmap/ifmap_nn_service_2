package utilities;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.filechooser.FileSystemView;

import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.ImageTransform;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.json.simple.JSONObject;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.factory.Nd4j;

import aaplication.IFMAP_Service;
import jcanny.JCanny;
import neural_network.ComputationGraphParams;
import neural_network.ModelUtils;
import neural_network.MultiLayerNetworkParams;
import pictureUtils.PictureReader;

public class FileUtils {

	public static int getNbOfFiles(File file) {
		int sub_files = 0;
		if (!file.isDirectory())
			return 1;
		for (File f : file.listFiles()) {
			sub_files += getNbOfFiles(f);
		}
		return sub_files;
	}

	public static int getNbOfUpFolders(File file) {
		if(file == null)
			return 0;
		
		int folders = 0;
		for (File f : file.listFiles()) {
			if (f != null && f.isDirectory())
				folders++;
		}
		return folders;
	}

	public static File getFileInChildren(File file, String contains) {
		for (File f : file.listFiles()) {
			if (f.getName().contains(contains))
				return f;
		}
		return null;
	}
	
	public static File loadFileOnDesktop(String fileName) {
		String desktopPath = System.getProperty("user.home") + "\\Desktop\\";
		String filePath = desktopPath + fileName;
//		System.out.println(filePath);
		return new File(filePath);
	}
	
	public static File loadFileFromBackend(String fileName) {
		return new File(IFMAP_Service.backendPublic + fileName);
	}
	
	public static DataSetIterator createDataSetIterator(File file, MultiLayerNetworkParams network, int batchSize,
			int maxPathPerLabel, ImageTransform transform) throws IOException {
		DataSetIterator dataIterator = null;
		/**
		 * cd Data Setup -> organize and limit data file paths: - mainPath = path to
		 * image files - fileSplit = define basic dataset split with limits on format -
		 * pathFilter = define additional file load filter to limit size and balance
		 * batch content
		 **/
		ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
		// System.out.println(labelMaker.getLabelForPath(PictureReader.TRAINING_FOLDER +
		// "\\Apple Braeburn\\0_100.jpg"));
		Random rng = new Random(network.getSeed());
		FileSplit fileSplit = new FileSplit(file, NativeImageLoader.ALLOWED_FORMATS, rng);
		int nbExamples = (int) fileSplit.length();
		System.out.println(nbExamples);
		BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, nbExamples, network.getnLabels(),
				maxPathPerLabel);

		DataNormalization scaler = new ImagePreProcessingScaler(0, 1);

		ImageRecordReader recordReader = new ImageRecordReader(network.getRows(), network.getColumns(),
				network.getNbOfChannels(), labelMaker);

		recordReader.initialize(fileSplit, transform);
		dataIterator = new RecordReaderDataSetIterator(recordReader, batchSize, 1, network.getnLabels());
		scaler.fit(dataIterator);
		dataIterator.setPreProcessor(scaler);

		return dataIterator;
	}
	
	public static DataSetIterator createDataSetIterator(File file, ComputationGraphParams network, int batchSize,
			int maxPathPerLabel, ImageTransform transform) throws IOException {
		DataSetIterator dataIterator = null;
		/**
		 * cd Data Setup -> organize and limit data file paths: - mainPath = path to
		 * image files - fileSplit = define basic dataset split with limits on format -
		 * pathFilter = define additional file load filter to limit size and balance
		 * batch content
		 **/
		ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
		// System.out.println(labelMaker.getLabelForPath(PictureReader.TRAINING_FOLDER +
		// "\\Apple Braeburn\\0_100.jpg"));
		Random rng = new Random(network.getSeed());
		FileSplit fileSplit = new FileSplit(file, NativeImageLoader.ALLOWED_FORMATS, rng);
		int nbExamples = (int) fileSplit.length();
		System.out.println(nbExamples);
		BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, nbExamples, network.getnLabels(),
				maxPathPerLabel);

		DataNormalization scaler = new ImagePreProcessingScaler(0, 1);

		ImageRecordReader recordReader = new ImageRecordReader(network.getRows(), network.getColumns(),
				network.getNbOfChannels(), labelMaker);

		recordReader.initialize(fileSplit, transform);
		dataIterator = new RecordReaderDataSetIterator(recordReader, batchSize, 1, network.getnLabels());
		scaler.fit(dataIterator);
		dataIterator.setPreProcessor(scaler);

		return dataIterator;
	}

	public static DataSetIterator createFFDataSetIte(File parentFile, int nSamples, int nRows, int nCols, int nLabels, int nbOfChannels, int batchSize)
			throws IOException {
		File folder = parentFile;
		File[] digitFolders = folder.listFiles();

		NativeImageLoader nil = new NativeImageLoader(nRows, nCols);
		ImagePreProcessingScaler scaler = new ImagePreProcessingScaler(0, 1);

		INDArray input = Nd4j.create(new int[] { nSamples, nRows * nCols * nbOfChannels });
		INDArray output = Nd4j.create(new int[] { nSamples, nLabels });

		int n = 0;
		// scan all 0..9 digit subfolders
		for (File digitFolder : digitFolders) {
			// take note of the digit in processing, since it will be used as a label
			int labelDigit = PictureReader.getIndexOfPictureName(digitFolder.getName());
			// scan all the images of the digit in processing
			File[] imageFiles = digitFolder.listFiles();
			for (File imageFile : imageFiles) {
				BufferedImage image = ImageIO.read(imageFile);
				image = ModelUtils.checkImageSize(image, image.getWidth(), image.getHeight(), nCols, nRows);
				// read the image as a one dimensional array of 0..255 values
				INDArray img = nil.asRowVector(imageFile);
				// scale the 0..255 integer values into a 0..1 floating range
				// Note that the transform() method returns void, since it updates its input
				// array
				scaler.transform(img);
				// copy the img array into the input matrix, in the next row
				input.putRow(n, img);
				// in the same row of the output matrix, fire (set to 1 value) the column
				// correspondent to the label
				output.put(n, labelDigit, 1.0);
				// row counter increment
				n++;
			}
		}

		//Join input and output matrixes into a dataset
		DataSet dataSet = new DataSet(input, output);
		//Convert the dataset into a list
		List<DataSet> listDataSet = dataSet.asList();
		//Shuffle its content randomly
		Collections.shuffle(listDataSet, new Random(System.currentTimeMillis()));
		//Build and return a dataset iterator that the network can use
		DataSetIterator dsi = new ListDataSetIterator<DataSet>(listDataSet, batchSize);
		return dsi;
	}

	/**
	 * Returns the file size in bytes, kilobytes, megabytes, gigabytes, etc.
	 * 
	 * @param f    The file to get the size
	 * @param type The type in which you want the size (bytes (0), kilobytes (1),
	 *             megabytes (2), gigabytes (3)
	 * @return The file size as a long in the desired format
	 */
	public static double fileSize(File f, int type) {
		if (type >= 0) {
			return fileSize(f, type - 1) / 1024.0;
		}
		return f.length();
	}
	
	public static boolean exportPicture(BufferedImage img, String extension, File exportFile) {
		boolean worked = false;
        try {
			worked = ImageIO.write(img, extension, exportFile);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Couldn't export img: " + img);
		}
        return worked;
	}
	
	public static boolean removeFile(String path) {
		return new File(path).delete();
	}
	
	public static String writeResultsToJSON(String[][] results, String path) {
		JSONObject obj = new JSONObject();
		
		for(int i = 0; i < results.length; i++) {
			for(int j = 0; j < results[0].length; j++) {
				obj.put(results[i][0], results[i][j]);
			}
		}
		
		try (FileWriter file = new FileWriter(path)) {
			file.write(obj.toJSONString());
			System.out.println("\nJSON Object: " + obj);
			file.flush();
			file.close();
			return obj.toJSONString();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Couldn't write JSON");
			return "";
		}
	}
}
