package utilities;

import org.nd4j.evaluation.regression.RegressionEvaluation;

/**
 * Classe permettant d'enregistrer une Evaluation de R�gression d'un r�seau de neurones et d'autres
 * param�tres importants
 * @author Thomas Tr�panier
 *
 */
public class CustomRegressionEvaluation {

	private RegressionEvaluation evaluation;
	private long performedAtMilli;
	
	/**
	 * @return the evaluation
	 */
	public RegressionEvaluation getEvaluation() {
		return evaluation;
	}
	/**
	 * @param evaluation the evaluation to set
	 */
	public void setEvaluation(RegressionEvaluation evaluation) {
		this.evaluation = evaluation;
	}
	/**
	 * @return the performedAtMilli
	 */
	public long getPerformedAtMilli() {
		return performedAtMilli;
	}
	/**
	 * @param performedAtMilli the performedAtMilli to set
	 */
	public void setPerformedAtMilli(long performedAtMilli) {
		this.performedAtMilli = performedAtMilli;
	}
}
