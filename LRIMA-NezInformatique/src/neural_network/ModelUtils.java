package neural_network;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;

import javax.imageio.ImageIO;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.evaluation.regression.RegressionEvaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;

import aaplication.IFMAP;
import aaplication.IFMAP_Service;
import net.coobird.thumbnailator.Thumbnails;
import utilities.FileUtils;
import utilities.Utils;
import zcustom_nn.Layer;
import zcustom_nn.StatUtil;
import zcustom_nn.TrainingData;

public class ModelUtils {
	
	public static final String MODEL_SAVE_PATH = getModelSavePath();
	private static final String SAVE_PATH = "results\\";
	static final String RESULTS_FILE = "DL4J-Results.xlsx";
	static final String TRAINED_SAVE_PATH = "Trained Networks\\";
	static final String RESIZED_SAVE_PATH = "Data\\ResizedPictures\\resized.jpg";
	
	static final int DEFAULT_IMAGE_WIDTH = 100;
	static final int DEFAULT_IMAGE_HEIGHT = 100;
	static final int DEFAULT_NB_CHANNELS = 3;
	
	public static void trainModel(MultiLayerNetwork model, DataSet trainingSet, int epochs) {
		for (int i = 0; i < epochs; i++) {
			model.fit(trainingSet);
		}
	}

	public static void trainModel(MultiLayerNetwork model, DataSetIterator trainingSet, int epochs) {
		model.fit(trainingSet, epochs);
	}

	public static void trainModel(ComputationGraph graph, DataSetIterator trainingSet, int epochs) {
		graph.fit(trainingSet, epochs);
	}
	
	public static Evaluation getEvaluation(MultiLayerNetwork model, DataSetIterator testData, boolean showMatrix, boolean showEval) throws FileNotFoundException, IOException {
		System.out.println("Evaluate model...");
		Evaluation eval = ModelUtils.evaluateModel(model, testData, showMatrix, showEval);

		return eval;
	}
	
	public static Evaluation evaluateModel(MultiLayerNetwork model, DataSet testSet, boolean showMatrix, boolean showEval) {
		INDArray outputs = model.output(testSet.getFeatures());
		Evaluation eval = new Evaluation();
		eval.eval(testSet.getLabels(), outputs);
		if(showEval)
			System.out.println(eval.stats(true, showMatrix));
		return eval;
	}

	public static Evaluation evaluateModel(MultiLayerNetwork model, DataSetIterator testSet, boolean showMatrix, boolean showEval) {
		Evaluation eval = model.evaluate(testSet);
		if(showEval)
			System.out.println(eval.stats(true, showMatrix));
		return eval;
	}
	
	public static Evaluation evaluateGraph(ComputationGraph graph, DataSetIterator testSet, boolean showMatrix, boolean showEval) {
		Evaluation eval = graph.evaluate(testSet);
		if(showEval)
			System.out.println(eval.stats(true, showMatrix));
		
		return eval;
	}
	
	public static void saveModel(MultiLayerNetwork model, int nOut, int[] nHidden, double accuracy) throws IOException {
		Timestamp ts = new Timestamp(System.currentTimeMillis());
		String layersInfo = "";
		for (int layer : nHidden) {
			layersInfo += layer;
		}
		String fileName = StatUtil.normalizeFileName(layersInfo + "_" + ts + "_" + accuracy);

		File locationToSave = new File(MODEL_SAVE_PATH + fileName + ".zip"); // Where to save the network. Note: the file is
		boolean saveUpdater = true; // Updater: i.e., the state for Momentum, RMSProp, Adagrad etc. Save this if you
									// want to train your network more in the future
		model.save(locationToSave, saveUpdater);
	}
	
	public static void saveModel(ComputationGraph graph, String modelName, int nOut, double accuracy) throws IOException {
		File locationToSave = Utils.getLocationToSave(MODEL_SAVE_PATH, modelName, nOut, accuracy);
		boolean saveUpdater = true; // Updater: i.e., the state for Momentum, RMSProp, Adagrad etc. Save this if you
									// want to train your network more in the future
		graph.save(locationToSave, saveUpdater);
	}
	
	public static void saveModel(MultiLayerNetwork model, String modelName, int nOut, double accuracy) throws IOException {
		File locationToSave = Utils.getLocationToSave(MODEL_SAVE_PATH, modelName, nOut, accuracy);
		boolean saveUpdater = true; // Updater: i.e., the state for Momentum, RMSProp, Adagrad etc. Save this if you
									// want to train your network more in the future
		model.save(locationToSave, saveUpdater);
	}

	public static void saveModelResults(String sheetName, String[] headers, Object[] valuesToWrite) throws FileNotFoundException, IOException, EncryptedDocumentException, InvalidFormatException {
		File file = new File(SAVE_PATH + RESULTS_FILE);

		System.out.println("Writing... " + sheetName);
		try (InputStream inp = new FileInputStream(file)) {
			Workbook workbook = WorkbookFactory.create(inp);
			XSSFSheet sheet = (XSSFSheet) workbook.getSheet(sheetName);
			if (sheet == null) {
				sheet = (XSSFSheet) workbook.createSheet(sheetName);
			}

			//Creates the title for each column
			if (sheet.getRow(0) == null)
				initializeHeaders(sheet, headers);
			
			//Writes the values in each column
			int startRow = findFirstFreeRow(sheet);
			writeValues(sheet, startRow, valuesToWrite);
			
			try {
				FileOutputStream outputStream = new FileOutputStream(file);
				workbook.write(outputStream);
				workbook.close();
				outputStream.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private static void initializeHeaders(XSSFSheet sheet, String[] headers) {
		Row row = sheet.createRow(0);
		for(int i = 0; i < headers.length; i++) {
			Cell cell = row.createCell(i);
			cell.setCellValue(headers[i]);
		}
	}
	
	private static int findFirstFreeRow(XSSFSheet sheet) {
		int row = 0;
		Cell currentCell = sheet.getRow(row).getCell(0);
		while(currentCell != null) {
			row++;
			if(sheet.getRow(row) == null)
				return row;
			currentCell = sheet.getRow(row).getCell(0);
		}
		return row;
	}
	
	private static void writeValues(XSSFSheet sheet, int startRow, Object[] values) {
		Row row = sheet.createRow(startRow);
		for(int i = 0; i < values.length; i++) {
			Cell cell = row.createCell(i);
			try { //Try to write the value as Double, if not, then try as Int, if not, then try as String
				cell.setCellValue((double) values[i]);
			} catch (Exception e) {
				try {
					cell.setCellValue((int) values[i]); 
				} catch (Exception e2) {
					cell.setCellValue(values[i] + "");
				}
			}
		}
	}
	
	public static boolean saveEvaluationModel(MultiLayerNetwork model, String aliment, double accuracy, int examples) {
		String name = "evaluate-" + aliment + "-" + accuracy + "-" + examples;

		File locationToSave = new File(TRAINED_SAVE_PATH + name + ".zip"); // Where to save the network. Note: the file is in
																	// .zip format - can be opened externally
		boolean saveUpdater = true; // Updater: i.e., the state for Momentum, RMSProp, Adagrad etc. Save this if you
									// want to train your network more in the future
		try {
			model.save(locationToSave, saveUpdater);
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public static MultiLayerNetwork loadModel(ModelJob job, String aliment, int nbLabels, boolean loadUpdater, boolean isBackend) throws NullPointerException, IOException{
		File savedModel = getModelFile(job, aliment, nbLabels, isBackend);
		return MultiLayerNetwork.load(savedModel, loadUpdater);
	}
	
	public static File getModelFile(ModelJob job, String aliment, int nbLabels, boolean isBackend) {
		String modelName = getSavedName(job, aliment, nbLabels);
		System.out.println(modelName);
		if(modelName.equals("")) {
			return null;
		}
		
		File parentFolder = null;
		if(isBackend) {
			parentFolder = FileUtils.loadFileFromBackend(TRAINED_SAVE_PATH);
		} else {
			parentFolder = new File(TRAINED_SAVE_PATH); 
		}
		
		for(File f : parentFolder.listFiles()) {
			if(f.getName().contains(modelName))
				return f;
		}
		return null;
	}
	
	public static String getSavedName(ModelJob job, String aliment, int nbLabels) {
		if(job.equals(ModelJob.RECOGNIZE))
			return "recognize" + nbLabels;
		else if(job.equals(ModelJob.EVALUATE))
			return IFMAP.EVALUTE_PATH_START + nbLabels + "-" + aliment;
		else
			return "";
	}
	
	public static INDArray passImage(BufferedImage img, MultiLayerNetwork model) {
//		System.out.println("IMG: " + img);
		INDArray output = null;
		
		int[] modelInputSize = getInputSize(model);
		
		// Main background thread, this will load the model and test the input image
		// The dimensions of the images are set here
		int height = img.getHeight();
		int width = img.getWidth();
		int channels = modelInputSize[0];
		
		if(height > modelInputSize[1] || width > modelInputSize[2]) {
			img = checkImageSize(img, width, height, modelInputSize[2], modelInputSize[1]);
			
			width = modelInputSize[2];
			height = modelInputSize[1];
		}

		// Now we load the model from the raw folder with a try / catch block
		try {
//			System.out.println("Native Loader");
			// Use the nativeImageLoader to convert to numerical matrix
			NativeImageLoader loader = new NativeImageLoader(height, width, channels);
			// put image into INDArray
//			System.out.println("INDArray");
			INDArray image = loader.asMatrix(img);
			// values need to be scaled
			ImagePreProcessingScaler scalar = new ImagePreProcessingScaler(0, 1);
//			System.out.println("Scalar");
			// then call that scalar on the image dataset
			scalar.transform(image);
			
			// pass through neural net and store it in output array
//			System.out.println("Output");
			output = model.output(image);

		} catch (Exception e) {
			System.out.println("Couldn't output image");
			e.printStackTrace();
		}
		return output;
	}
	
	private static int[] getInputSize(MultiLayerNetwork model) {
		int[] modelSize = {DEFAULT_NB_CHANNELS, DEFAULT_IMAGE_HEIGHT, DEFAULT_IMAGE_WIDTH};
		
		String json = model.getLayerWiseConfigurations().toJson();
		
		if(json.indexOf("inputHeight") != -1) {
			String fromInputHeight = json.substring(json.indexOf("inputHeight"));
			String inputHeight = fromInputHeight.substring(fromInputHeight.indexOf(": ") + 2, fromInputHeight.indexOf(","));
//			System.out.println(inputHeight);
			
			String fromInputWidth = json.substring(json.indexOf("inputWidth"));
			String inputWidth = fromInputWidth.substring(fromInputWidth.indexOf(": ") + 2, fromInputWidth.indexOf(","));
//			System.out.println(inputWidth);
			
			String fromNumChannels = json.substring(json.indexOf("numChannels"));
			String numChannels = fromNumChannels.substring(fromNumChannels.indexOf(": ") + 2, fromNumChannels.indexOf(": ") + 3);
//			System.out.println(numChannels);
			
			modelSize[0] = Integer.parseInt(numChannels);
			modelSize[1] = Integer.parseInt(inputHeight);
			modelSize[2] = Integer.parseInt(inputHeight);
		}
		
		return modelSize;
	}
    
	public static BufferedImage checkImageSize(BufferedImage img, int width, int height, int targetWidth, int targetHeight) {
		BufferedImage resizedImg = null;
		//Try to fit the image if it is to big
		try {
			resizedImg = Thumbnails.of(img).size(targetWidth, targetHeight).asBufferedImage();
//			boolean successfulExport = FileUtils.exportPicture(resizedImg, "png", new File(RESIZED_SAVE_PATH));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resizedImg;
	}
	
	private static String getModelSavePath() {
		String desktopPath = System.getProperty("user.home") + "\\Desktop\\";
		return desktopPath + "Data_IFMAP\\Networks\\";
	}
	
	/**
	 * Performe et retourne une �valuation du MultiLayerNetwork 
	 * @param network Le mod�le � �valuer
	 * @param testSet Le DataSet � utiliser pour l'�valuation
	 * @param showMatrix Doit-on afficher la matrice de confusion
	 * @param showEval Doit-on afficher l'�valuation
	 * @return L'�valuation du network
	 */
	public static Evaluation getEvaluationForModel(MultiLayerNetwork network, DataSetIterator testSet, boolean showMatrix, boolean showEval) {
		Evaluation eval = network.evaluate(testSet);
		if(showEval)
			System.out.print(eval.stats(true, showMatrix));
		return eval;
	}

	/**
	 * Performe et retourne une �valuation du ComputationGraph 
	 * @param network Le mod�le � �valuer
	 * @param testSet Le DataSet � utiliser pour l'�valuation
	 * @param showMatrix Doit-on afficher la matrice de confusion
	 * @param showEval Doit-on afficher l'�valuation
	 * @return L'�valuation du network
	 */
	public static Evaluation getEvaluationForModel(ComputationGraph network, DataSetIterator testSet, boolean showMatrix, boolean showEval) {
		Evaluation eval = network.evaluate(testSet);
		if(showEval)
			System.out.print(eval.stats(true, showMatrix));
		return eval;
	}
	
	/**
	 * Performe et retourne une �valuation de r�gression du MultiLayerNetwork 
	 * @param network Le mod�le � �valuer
	 * @param testSet Le DataSet � utiliser pour l'�valuation
	 * @param showMatrix Doit-on afficher la matrice de confusion
	 * @param showEval Doit-on afficher l'�valuation
	 * @return L'�valuation du network
	 */
	public static RegressionEvaluation getRegressionEvaluationForModel(MultiLayerNetwork network, DataSetIterator testSet, boolean showEval) {
		RegressionEvaluation eval = network.evaluateRegression(testSet);
		if(showEval)
			System.out.print(eval.stats());
		return eval;
	}

	/**
	 * Performe et retourne une �valuation de r�gression du ComputationGraph 
	 * @param network Le mod�le � �valuer
	 * @param testSet Le DataSet � utiliser pour l'�valuation
	 * @param showMatrix Doit-on afficher la matrice de confusion
	 * @param showEval Doit-on afficher l'�valuation
	 * @return L'�valuation du network
	 */
	public static RegressionEvaluation getRegressionEvaluationForModel(ComputationGraph network, DataSetIterator testSet, boolean showEval) {
		RegressionEvaluation eval = network.evaluateRegression(testSet);
		if(showEval)
			System.out.print(eval.stats());
		return eval;
	}
}
