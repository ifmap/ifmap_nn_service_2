package neural_network;

import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import org.apache.commons.compress.utils.Lists;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.CollectScoresIterationListener;
import org.deeplearning4j.optimize.listeners.PerformanceListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import Listeners.ExcelSaveListener;
import Listeners.ModelSaveListener;
import models.ANNs;
import utilities.FileUtils;
import utilities.Utils;

import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
public class RecognitionFF {
	
	static final String MAIN_PATH = "Data_IFMAP\\Fruits\\";
	static final String TRAINING_FOLDER = "Fruit_Training_FF\\";
	static final String TEST_FOLDER = "Fruit_Test_FF\\";
	static String SAVE_SHEET = "Recog_FF";
	static String SCORE_EXPORT = "RecogFF";
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		File trainingFolder = FileUtils.loadFileOnDesktop(MAIN_PATH + TRAINING_FOLDER); 
		File testFolder = FileUtils.loadFileOnDesktop(MAIN_PATH + TEST_FOLDER); 
		int nbOfTrainingSamples = FileUtils.getNbOfFiles(trainingFolder);
		int nbOfTestSamples = FileUtils.getNbOfFiles(testFolder);
		
		int nRows = 100; //MNIST format
		int nCols = 100; //MNIST format
		int nbOfChannels = 3;
		
		int seed = 123; //Random value to shuffle the data
		double learningRate = 0.05;
		double momentum = 0.75;
		int batchSize = 50;
		int epochs = 20;
		int nIn = nRows * nCols * nbOfChannels;
		int[] nHidden = {32, 16};
		int nOut = 3;
		int maxPathPerLabel = 1000;
		
		int nbFoldersTrain = FileUtils.getNbOfUpFolders(trainingFolder);
		int nbFoldersTest = FileUtils.getNbOfUpFolders(testFolder);

		if (nbFoldersTest != nbFoldersTrain) {
			System.out.println(
					"Numbers of fruits in train not same as in test " + nbFoldersTrain + " / " + nbFoldersTest);
			System.exit(0);
		}
		
		nOut = nbFoldersTest;
		int nbLabels = nOut;
//		SAVE_SHEET += nbLabels;
		System.out.println(nOut);
		
		System.out.println("Loading dataset...");
		DataSetIterator trainingSetIterator = null;
		DataSetIterator testSetIterator = null;
		
		//MNIST
		/*try {
			trainingSetIterator = new MnistDataSetIterator(batchSize, true, seed);
			testSetIterator = new MnistDataSetIterator(batchSize, true, seed);
			nIn = trainingSetIterator.inputColumns();
			nOut = trainingSetIterator.totalOutcomes();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/

		trainingSetIterator = FileUtils.createFFDataSetIte(trainingFolder, nbOfTrainingSamples, nRows, nCols, nbLabels, nbOfChannels, batchSize);
		testSetIterator = FileUtils.createFFDataSetIte(testFolder, nbOfTestSamples, nRows, nCols, nbLabels, nbOfChannels, batchSize);
		
		System.out.println("Configuring network...");
		MultiLayerNetwork network = ANNs.initializeFFModel(seed, learningRate, momentum, nIn, nHidden, nbLabels);
		MultiLayerNetworkParams model = new MultiLayerNetworkParams(network, seed, batchSize, nbOfChannels, network.getnLayers(), nbLabels, nRows, nCols, NNType.ANN, "FFA");
		model.setRows(nRows);
		model.setColumns(nCols);
		model.setNbOfChannels(nbOfChannels);
	    System.out.println(network.summary());
	    
		//LISTENERS
		CollectScoresIterationListener collectionScoreListener = new CollectScoresIterationListener(50);
		collectionScoreListener.iterationDone((Model) (model.getModel()), 50, 1);
		ExcelSaveListener excelListener = new ExcelSaveListener(SAVE_SHEET, testSetIterator, System.currentTimeMillis(), model, true, false);
		ModelSaveListener saveListener = new ModelSaveListener(SAVE_SHEET, nOut, testSetIterator);
		model.getModel().setListeners(collectionScoreListener, excelListener, saveListener);
		
		System.out.println("Training model...");
		long startTime = System.currentTimeMillis();
		ModelUtils.trainModel(model.getModel(), trainingSetIterator, epochs);

		long trainTime = (System.currentTimeMillis() - startTime) / 1000;
		System.out.println("Time to train: " + trainTime + "s");
		Evaluation eval = ModelUtils.evaluateModel(model.getModel(), testSetIterator, false, true);
		
		//LOCK OR UNLOCK DON'T FORGET!!!
		collectionScoreListener.exportScores(new File(SCORE_EXPORT + epochs + ".txt"));
		JOptionPane.showMessageDialog(null, "Done");
	}
}
