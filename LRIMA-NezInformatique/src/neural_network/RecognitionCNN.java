package neural_network;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.FlipImageTransform;
import org.datavec.image.transform.ImageTransform;
import org.datavec.image.transform.PipelineImageTransform;
import org.datavec.image.transform.WarpImageTransform;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.impl.MnistDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.distribution.Distribution;
import org.deeplearning4j.nn.conf.distribution.GaussianDistribution;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.LocalResponseNormalization;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.CheckpointListener;
import org.deeplearning4j.optimize.listeners.CollectScoresIterationListener;
import org.deeplearning4j.optimize.listeners.PerformanceListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.zoo.*;
import org.deeplearning4j.zoo.model.AlexNet;
import org.deeplearning4j.zoo.model.LeNet;
import org.deeplearning4j.zoo.model.VGG16;
import org.nd4j.autodiff.listeners.impl.ScoreListener;
import org.nd4j.common.primitives.Pair;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ops.LossFunction;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.learning.config.AdaDelta;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.schedule.MapSchedule;
import org.nd4j.linalg.schedule.ScheduleType;

import Listeners.ExcelSaveListener;
import Listeners.ModelSaveListener;
import pictureUtils.PictureReader;
import utilities.FileUtils;
import utilities.Utils;
import models.*;
import models.CNNs;

import org.nd4j.linalg.lossfunctions.LossFunctions;

public class RecognitionCNN {

	static final String MAIN_PATH = "Data_IFMAP\\Fresh-Rotten\\";
	static final String TRAINING_FOLDER = "train-recog\\";
	static final String TEST_FOLDER = "test-recog\\";
	static String SCORE_EXPORT = "results\\scoreExportCNNNegative";
	static final String SAVE_PATH = "results\\";
	static String SAVE_SHEET = "Recog-Fresh-CNN-";

	static int maxPathPerLabel = 1000;
	static double splitTrainTest = 0.8;
	static int nRows = 100; // MNIST format
	static int nCols = 100; // MNIST format
	static int nbOfChannels = 3;
	static int seed = 123; // Random value to shuffle the data
	static int batchSize = 50;
	static int nIn = nRows * nCols;
	static int epochs = 50;
	static Random rng = new Random(seed);
	
	static File trainingFile;
	static File testFile;
	
	static final String TEST_PICTURE = "Data\\Fresh-Rotten\\apple.png";
	public static void main(String[] args) throws IOException {
		trainingFile = FileUtils.loadFileOnDesktop(MAIN_PATH + TRAINING_FOLDER);
		testFile = FileUtils.loadFileOnDesktop(MAIN_PATH + TEST_FOLDER);
		
		runCC(0);
	}
	
	public static void runCC(int param) throws IOException {
		int nbFoldersTrain = FileUtils.getNbOfUpFolders(trainingFile);
		int nbFoldersTest = FileUtils.getNbOfUpFolders(testFile);

		if (nbFoldersTest != nbFoldersTrain) {
			System.out.println(
					"Numbers of fruits in train not same as in test " + nbFoldersTrain + " / " + nbFoldersTest);
			System.exit(0);
		}
		
		int nOut = nbFoldersTest;
		int nbLabels = nOut;
		SAVE_SHEET = "Recog-CNN-3-LRIMA";
		System.out.println(nOut);

		System.out.println("Loading dataset...");
		DataSetIterator trainingSetIterator = null;
		DataSetIterator testSetIterator = null;

		System.out.println("Configuring network...");
		MultiLayerNetworkParams model = CNNs.lenetModel(seed, batchSize, nbOfChannels, nbLabels, nRows, nCols);
		
//		ComputationGraphParams model = new ComputationGraphParams(graph, seed, nbOfChannels, graph.getNumLayers(), nbLabels, net.getInputShape()[1], net.getInputShape()[2], NNType.GRAPH, "NASNet");
		
		System.out.println(model.getModel().summary());
		
		ImageTransform transform = null;
        /*
         * Data Setup -> transformation
         *  - Transform = how to transform images and generate large dataset to train on
         */
        ImageTransform flipTransform1 = new FlipImageTransform(rng);
        ImageTransform flipTransform2 = new FlipImageTransform(new Random(123));
        ImageTransform warpTransform = new WarpImageTransform(rng, 42);
        boolean shuffle = false;
        List<Pair<ImageTransform,Double>> pipeline = Arrays.asList(new Pair<>(flipTransform1,0.9),
                                                                   new Pair<>(flipTransform2,0.8),
                                                                   new Pair<>(warpTransform,0.5));
        
        //noinspection ConstantConditions
        transform = new PipelineImageTransform(pipeline,shuffle);
        
		trainingSetIterator = FileUtils.createDataSetIterator(trainingFile, model, batchSize, maxPathPerLabel, null);
		System.out.println("Labels: " + trainingSetIterator.getLabels());
		testSetIterator = FileUtils.createDataSetIterator(testFile, model, batchSize, maxPathPerLabel, null);
		
		//LISTENERS
		CollectScoresIterationListener collectionScoreListener = new CollectScoresIterationListener(50);
		collectionScoreListener.iterationDone((Model) (model.getModel()), 50, 1);
		ExcelSaveListener excelListener = new ExcelSaveListener(SAVE_SHEET, testSetIterator, System.currentTimeMillis(), model, false, false);
		ModelSaveListener saveListener = new ModelSaveListener(SAVE_SHEET + "_" + nOut, nOut, testSetIterator);
		model.getModel().setListeners(collectionScoreListener, excelListener, saveListener);
		
		System.out.println("Training network...");
		long startTime = System.currentTimeMillis();
		ModelUtils.trainModel(model.getModel(), trainingSetIterator, epochs);

		long trainTime = (System.currentTimeMillis() - startTime) / 1000;
		System.out.println("Time to train: " + trainTime + "s");
		Evaluation eval = ModelUtils.evaluateModel(model.getModel(), testSetIterator, false, true);
		
		//LOCK OR UNLOCK DON'T FORGET!!!
		collectionScoreListener.exportScores(new File(SCORE_EXPORT + param + ".txt"));
	}
}
