package neural_network;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

public class MultiLayerNetworkParams extends NeuralNetworkParams {
	
	private MultiLayerNetwork network;
	
	public MultiLayerNetworkParams(MultiLayerNetwork network,int seed, int batchSize, int nbOfChannels, int nLayers, int nLabels, int h, int w, NNType nnType, String NNConfName) {
		super(seed, batchSize, nbOfChannels, nLayers, nLabels, h, w, nnType, NNConfName);
		setModel(network);
		getModel().init();
	}

	@Override
	public MultiLayerNetwork getModel() {
		return network;
	}

	@Override
	public void setModel(Model model) {
		MultiLayerNetwork net = null;
		try {
			net = (MultiLayerNetwork) model;
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		this.network = net;
	}
}
