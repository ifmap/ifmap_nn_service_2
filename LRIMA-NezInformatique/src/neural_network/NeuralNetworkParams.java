package neural_network;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.graph.ComputationGraph;

public abstract class NeuralNetworkParams {
	
	protected static final long serialVersionUID = 1L;
	protected int seed;
	protected int batchSize;
	protected int nbOfChannels;
	protected int nLayers;
	protected int nLabels;
	protected int rows;
	protected int columns;
	protected String NNConfName;
	protected NNType nnType;
	
	public NeuralNetworkParams(int seed, int batchSize, int nbOfChannels, int nLayers, int nLabels, int h, int w, NNType nnType, String NNConfName) {
		this.seed = seed;
		this.batchSize = batchSize;
		this.nbOfChannels = nbOfChannels;
		setnLayers(nLayers);
		this.nLabels = nLabels;
		this.rows = h;
		this.columns = w;
		this.setNNConfName(NNConfName);
		this.setNnType(nnType);
	}

	/**
	 * @return the seed
	 */
	public int getSeed() {
		return seed;
	}

	/**
	 * @param seed the seed to set
	 */
	public void setSeed(int seed) {
		this.seed = seed;
	}

	/**
	 * @return the batchSize
	 */
	public int getBatchSize() {
		return batchSize;
	}

	/**
	 * @param batchSize the batchSize to set
	 */
	public void setBatchSize(int batchSize) {
		this.batchSize = batchSize;
	}

	/**
	 * @return the nbOfChannels
	 */
	public int getNbOfChannels() {
		return nbOfChannels;
	}

	/**
	 * @param nbOfChannels the nbOfChannels to set
	 */
	public void setNbOfChannels(int nbOfChannels) {
		this.nbOfChannels = nbOfChannels;
	}

	/**
	 * @return the nLayers
	 */
	public int getnLayers() {
		return nLayers;
	}

	/**
	 * @param nLayers the nLayers to set
	 */
	public void setnLayers(int nLayers) {
		this.nLayers = nLayers;
	}

	/**
	 * @return the nLabels
	 */
	public int getnLabels() {
		return nLabels;
	}

	/**
	 * @param nLabels the nLabels to set
	 */
	public void setnLabels(int nLabels) {
		this.nLabels = nLabels;
	}

	/**
	 * @return the rows
	 */
	public int getRows() {
		return rows;
	}

	/**
	 * @param rows the rows to set
	 */
	public void setRows(int rows) {
		this.rows = rows;
	}

	/**
	 * @return the columns
	 */
	public int getColumns() {
		return columns;
	}

	/**
	 * @param columns the columns to set
	 */
	public void setColumns(int columns) {
		this.columns = columns;
	}

	/**
	 * @return the nNConfName
	 */
	public String getNNConfName() {
		return NNConfName;
	}

	/**
	 * @param nNConfName the nNConfName to set
	 */
	public void setNNConfName(String nNConfName) {
		NNConfName = nNConfName;
	}

	/**
	 * @return the nnType
	 */
	public NNType getNnType() {
		return nnType;
	}

	/**
	 * @param nnType the nnType to set
	 */
	public void setNnType(NNType nnType) {
		this.nnType = nnType;
	}
	
	public abstract Model getModel();
	public abstract void setModel(Model model);
}
