package neural_network;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

public class ComputationGraphParams extends NeuralNetworkParams{
	
	private ComputationGraph graph;
	
	public ComputationGraphParams(ComputationGraph graph, int seed, int batchSize, int nbOfChannels, int nLayers, int nLabels, int h, int w, NNType nnType, String NNConfName) {
		super(seed, batchSize, nbOfChannels, nLayers, nLabels, h, w, nnType, NNConfName);
		setModel(graph);
	}

	@Override
	public ComputationGraph getModel() {
		return graph;
	}

	@Override
	public void setModel(Model model) {
		ComputationGraph net = null;
		try {
			net = (ComputationGraph) model;
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		this.graph = net;
	}

}
