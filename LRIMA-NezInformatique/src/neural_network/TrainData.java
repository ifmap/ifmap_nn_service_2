package neural_network;

import java.io.File;

public class TrainData {
	
	private String name;
	private File trainFile;
	private File testFile;
	
	public TrainData(String name, File trainFile, File testFile) {
		setName(name);
		setTrainFile(trainFile);
		setTestFile(testFile);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the trainFile
	 */
	public File getTrainFile() {
		return trainFile;
	}

	/**
	 * @param trainFile the trainFile to set
	 */
	public void setTrainFile(File trainFile) {
		this.trainFile = trainFile;
	}

	/**
	 * @return the testFile
	 */
	public File getTestFile() {
		return testFile;
	}

	/**
	 * @param testFile the testFile to set
	 */
	public void setTestFile(File testFile) {
		this.testFile = testFile;
	}
}
