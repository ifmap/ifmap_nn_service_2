package neural_network;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.CropImageTransform;
import org.datavec.image.transform.FlipImageTransform;
import org.datavec.image.transform.ImageTransform;
import org.datavec.image.transform.PipelineImageTransform;
import org.datavec.image.transform.WarpImageTransform;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.listeners.CollectScoresIterationListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.factory.Nd4j;

import Listeners.ExcelSaveListener;
import Listeners.ModelSaveListener;
import models.SimpleCNN;
import models.Xception;
import models.CNNs;
import utilities.FileUtils;
import utilities.Utils;
import zcustom_nn.StatUtil;

public class EvaluationCNN {

	public static final String MAIN_PATH = "Data_IFMAP\\Fresh-Rotten";
	public static String TRAINING_PATH = "\\train-banana";
	public static String TEST_PATH = "\\test-banana";
	public static String SAVE_SHEET = "Eval-CNN";
	static String SCORE_EXPORT = "results\\scoreExportApple";
	static final String SAVE_PATH = "results\\";

	static int maxPathPerLabel = 1000;
	static double splitTrainTest = 0.8;
	static int nRows = 100; // MNIST format
	static int nCols = 100; // MNIST format
	static int nbOfChannels = 3;
	static int seed = 123; // Random value to shuffle the data
	static int batchSize = 50;
	static int nIn = nRows * nCols;
	static int epochs = 40;
	static Random rng = new Random(seed);

	static File trainingFile;
	static File testFile;

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		SAVE_SHEET += getAlimentInPath();
		System.out.println(Nd4j.backend.BACKEND_PRIORITY_GPU);
		trainingFile = FileUtils.loadFileOnDesktop(MAIN_PATH + TRAINING_PATH);
		testFile = FileUtils.loadFileOnDesktop(MAIN_PATH + TEST_PATH);
		run(0);
		
		SAVE_SHEET = "Eval-CNN-apple";
		TRAINING_PATH = "\\train-apple";
		TEST_PATH = "\\test-apple";
		trainingFile = FileUtils.loadFileOnDesktop(MAIN_PATH + TRAINING_PATH);
		testFile = FileUtils.loadFileOnDesktop(MAIN_PATH + TEST_PATH);
		run(0);
	}

	private static void run(int param) throws IOException {
		// X = Param
		int nbFoldersTrain = FileUtils.getNbOfUpFolders(trainingFile);
		int nbFoldersTest = FileUtils.getNbOfUpFolders(testFile);

		if (nbFoldersTest != nbFoldersTrain) {
			System.out.println(
					"Numbers of fruits in train not same as in test " + nbFoldersTrain + " / " + nbFoldersTest);
			System.exit(0);
		}
		int nOut = nbFoldersTest;
		int nbLabels = nOut;

		System.out.println("Loading dataset...");
		DataSetIterator trainingSetIterator = null;
		DataSetIterator testSetIterator = null;

		System.out.println("Configuring network...");
		MultiLayerNetworkParams model = CNNs.alexnetModel(seed, batchSize, nbOfChannels, nbLabels, nRows, nCols);
		System.out.println(model.getModel().summary());

		trainingSetIterator = FileUtils.createDataSetIterator(trainingFile, model, batchSize, maxPathPerLabel, null);
		testSetIterator = FileUtils.createDataSetIterator(testFile, model, batchSize, maxPathPerLabel, null);

		System.out.println("Evaluate untrained model...");
		ModelUtils.evaluateModel(model.getModel(), testSetIterator, true, true);

		// LISTENERS
		CollectScoresIterationListener collectionScoreListener = new CollectScoresIterationListener(50);
		collectionScoreListener.iterationDone((Model) (model.getModel()), 50, 1);
		ExcelSaveListener excelListener = new ExcelSaveListener(SAVE_SHEET, testSetIterator, System.currentTimeMillis(), model, true, true);
		ModelSaveListener saveListener = new ModelSaveListener(SAVE_SHEET, nOut, testSetIterator);
		model.getModel().setListeners(collectionScoreListener, excelListener, saveListener);

		System.out.println("Training network...");
		long startTime = System.currentTimeMillis();
		ModelUtils.trainModel(model.getModel(), trainingSetIterator, epochs);

		long trainTime = (System.currentTimeMillis() - startTime) / 1000;
		System.out.println("Time to train: " + trainTime + "s");
		ModelUtils.evaluateModel(model.getModel(), testSetIterator, true, true);
		
		//LOCK OR UNLOCK DON'T FORGET!!!
		collectionScoreListener.exportScores(new File(SCORE_EXPORT + getAlimentInPath() + param + ".txt"));
	}

	private static String getAlimentInPath() {
		int hyphenId = TRAINING_PATH.indexOf('-');
		if(hyphenId == -1) 
			return "-All";
		
		return TRAINING_PATH.substring(hyphenId, TRAINING_PATH.length());
	}
}
