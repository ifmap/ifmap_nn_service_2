package models;

import org.deeplearning4j.common.resources.DL4JResources;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.distribution.Distribution;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.distribution.TruncatedNormalDistribution;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.zoo.ModelMetaData;
import org.deeplearning4j.zoo.PretrainedType;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.ZooType;
import org.nd4j.common.primitives.Pair;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.AdaDelta;
import org.nd4j.linalg.learning.config.IUpdater;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import static org.deeplearning4j.zoo.model.helper.NASNetHelper.normalA;
import static org.deeplearning4j.zoo.model.helper.NASNetHelper.reductionA;

/**
 * U-Net
 *
 * Implementation of NASNet-A in Deeplearning4j. NASNet refers to Neural Architecture Search Network, a family of models
 * that were designed automatically by learning the model architectures directly on the dataset of interest.
 *
 * <p>This implementation uses 1056 penultimate filters and an input shape of (3, 224, 224). You can change this.</p>
 *
 * <p>Paper: <a href="https://arxiv.org/abs/1707.07012">https://arxiv.org/abs/1707.07012</a></p>
 * <p>ImageNet weights for this model are available and have been converted from <a href="https://keras.io/applications/">https://keras.io/applications/</a>.</p>
 *
 * @note If using the IMAGENETLARGE weights, the input shape is (3, 331, 331).
 * @author Justin Long (crockpotveggies)
 *
 */
public class NASNet {
	
	private long seed = 1234;
    private int[] inputShape = new int[] {3, 224, 224};
    private int numClasses = 0;
    private WeightInit weightInit = WeightInit.RELU;
    private IUpdater updater = new AdaDelta();
    private CacheMode cacheMode = CacheMode.DEVICE;
    private WorkspaceMode workspaceMode = WorkspaceMode.ENABLED;
    private ConvolutionLayer.AlgoMode cudnnAlgoMode = ConvolutionLayer.AlgoMode.PREFER_FASTEST;

    // NASNet specific
    private int numBlocks = 6;
    private int penultimateFilters = 1056;
    private int stemFilters = 96;
    private int filterMultiplier = 2;
    private boolean skipReduction = true;
    
    public ComputationGraph nasNet(int nbLabels) {
    	this.numClasses = nbLabels;
    	ComputationGraph graph = conf();
    	graph.init();
    	
    	return graph;
    }
    
    public ComputationGraph nasNet(int seed, int nbChannels, int nbLabels) {
    	this.seed = seed;
    	this.inputShape[0] = nbChannels;
    	this.numClasses = nbLabels;
    	ComputationGraph graph = conf();
    	graph.init();
    	
    	return graph;
    }
    
    public ComputationGraph conf() {
        ComputationGraphConfiguration.GraphBuilder graph = graphBuilder();

        graph.addInputs("input").setInputTypes(InputType.convolutional(inputShape[2], inputShape[1], inputShape[0]));

        ComputationGraphConfiguration conf = graph.build();
        ComputationGraph model = new ComputationGraph(conf);
        model.init();

        return model;
    }
    
    public ComputationGraphConfiguration.GraphBuilder graphBuilder() {

        if(penultimateFilters % 24 != 0) {
            throw new IllegalArgumentException("For NASNet-A models penultimate filters must be divisible by 24. Current value is "+penultimateFilters);
        }
        int filters = (int) Math.floor(penultimateFilters / 24);

        ComputationGraphConfiguration.GraphBuilder graph = new NeuralNetConfiguration.Builder().seed(seed)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(updater)
                .weightInit(weightInit)
                .l2(5e-5)
                .miniBatch(true)
                .cacheMode(cacheMode)
                .trainingWorkspaceMode(workspaceMode)
                .inferenceWorkspaceMode(workspaceMode)
                .cudnnAlgoMode(cudnnAlgoMode)
                .convolutionMode(ConvolutionMode.Truncate)
                .graphBuilder();

        if(!skipReduction) {
            graph.addLayer("stem_conv1", new ConvolutionLayer.Builder(3, 3).stride(2, 2).nOut(stemFilters).hasBias(false)
                    .cudnnAlgoMode(cudnnAlgoMode).build(), "input");
        } else {
            graph.addLayer("stem_conv1", new ConvolutionLayer.Builder(3, 3).stride(1, 1).nOut(stemFilters).hasBias(false)
                    .cudnnAlgoMode(cudnnAlgoMode).build(), "input");
        }

        graph.addLayer("stem_bn1", new BatchNormalization.Builder().eps(1e-3).gamma(0.9997).build(), "stem_conv1");

        String inputX = "stem_bn1";
        String inputP = null;
        if(!skipReduction) {
            Pair<String, String> stem1 = reductionA(graph, (int) Math.floor(stemFilters / Math.pow(filterMultiplier,2)), "stem1", "stem_conv1", inputP);
            Pair<String, String> stem2 = reductionA(graph, (int) Math.floor(stemFilters / (filterMultiplier)), "stem2", stem1.getFirst(), stem1.getSecond());
            inputX = stem2.getFirst();
            inputP = stem2.getSecond();
        }

        for(int i = 0; i < numBlocks; i++){
            Pair<String, String> block = normalA(graph, filters, String.valueOf(i), inputX, inputP);
            inputX = block.getFirst();
            inputP = block.getSecond();
        }

        String inputP0;
        Pair<String, String> reduce = reductionA(graph, filters * filterMultiplier, "reduce"+numBlocks, inputX, inputP);
        inputX = reduce.getFirst();
        inputP0 = reduce.getSecond();

        if(!skipReduction) inputP = inputP0;

        for(int i = 0; i < numBlocks; i++){
            Pair<String, String> block = normalA(graph, filters * filterMultiplier, String.valueOf(i+numBlocks+1), inputX, inputP);
            inputX = block.getFirst();
            inputP = block.getSecond();
        }

        reduce = reductionA(graph, filters * (int)Math.pow(filterMultiplier, 2), "reduce"+(2*numBlocks), inputX, inputP);
        inputX = reduce.getFirst();
        inputP0 = reduce.getSecond();

        if(!skipReduction) inputP = inputP0;

        for(int i = 0; i < numBlocks; i++){
            Pair<String, String> block = normalA(graph, filters * (int) Math.pow(filterMultiplier, 2), String.valueOf(i+(2*numBlocks)+1), inputX, inputP);
            inputX = block.getFirst();
            inputP = block.getSecond();
        }

        // output
        graph
                .addLayer("act", new ActivationLayer(Activation.RELU), inputX)
                .addLayer("avg_pool", new GlobalPoolingLayer.Builder(PoolingType.AVG).build(), "act")
                .addLayer("output", new OutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                    .activation(Activation.SOFTMAX).build(), "avg_pool")

                .setOutputs("output")

                ;

        return graph;
    }
    
    public int[] getInputShape() {
    	return this.inputShape;
    }
}
