package models;

import java.util.HashMap;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration.ListBuilder;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.PerformanceListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.schedule.MapSchedule;
import org.nd4j.linalg.schedule.ScheduleType;

import neural_network.MultiLayerNetworkParams;
import neural_network.NNType;

public class ANNs {


	public static MultiLayerNetwork initializeFFModel(int seed, double learningRate, double momentum, int nIn, int[] nHidden, int nOut) {
		NeuralNetConfiguration.Builder builder = new NeuralNetConfiguration.Builder();
		builder.seed(seed)
		.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
		.updater(new Nesterovs(learningRate, momentum))
		.weightInit(WeightInit.XAVIER);
		
		ListBuilder list = builder.list();
		list.layer(new DenseLayer.Builder()
				.nIn(nIn)
				.nOut(nHidden[0])
				.activation(Activation.RELU)
				.build());
		
		for(int i = 1; i < nHidden.length; i++) {
			list.layer(new DenseLayer.Builder()
					.nIn(nHidden[i-1])
					.nOut(nHidden[i])
					.activation(Activation.RELU)
					.build());
		}
		
		list.layer(new OutputLayer.Builder()
				.nIn(nHidden[nHidden.length - 1])
				.nOut(nOut)
				.activation(Activation.SOFTMAX)
				.build());
		
		MultiLayerConfiguration conf = list.build();
		MultiLayerNetwork model = new MultiLayerNetwork(conf);
		model.init();
		return model;
	}
	
//	MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
//			.seed(seed)
//			.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
//			.updater(new Nesterovs(learningRate, momentum))
//			.weightInit(WeightInit.XAVIER)
//			.list()
//			.layer(new DenseLayer.Builder()
//					.nIn(nIn)
//					.nOut(nHidden[0])
//					.activation(Activation.RELU)
//					.build())
//			.layer(new DenseLayer.Builder()
//					.nIn(nHidden[0])
//					.nOut(nHidden[1])
//					.activation(Activation.RELU)
//					.build())
//			.layer(new OutputLayer.Builder()
//					.nIn(nHidden[1])
//					.nOut(nOut)
//					.activation(Activation.SOFTMAX)
//					.build())
//			.build();
}
