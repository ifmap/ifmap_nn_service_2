package models;

import java.util.HashMap;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.CacheMode;
import org.deeplearning4j.nn.conf.ConvolutionMode;
import org.deeplearning4j.nn.conf.GradientNormalization;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.WorkspaceMode;
import org.deeplearning4j.nn.conf.distribution.Distribution;
import org.deeplearning4j.nn.conf.distribution.GaussianDistribution;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.ConvolutionLayer;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.LocalResponseNormalization;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.conf.layers.SubsamplingLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.AdaDelta;
import org.nd4j.linalg.learning.config.IUpdater;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.linalg.schedule.MapSchedule;
import org.nd4j.linalg.schedule.ScheduleType;

import neural_network.NNType;
import neural_network.MultiLayerNetworkParams;

public class CNNs {

	public static MultiLayerNetwork initializeConvModel(int seed, int nbOfChannels, int[] nHidden, int nOut,
			HashMap<Integer, Double> learningRateSchedule, int row, int col) {
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed).l2(0.0005)
				.updater(new Nesterovs(new MapSchedule(ScheduleType.ITERATION, learningRateSchedule)))
				.weightInit(WeightInit.XAVIER).list()
				.layer(new ConvolutionLayer.Builder(5, 5).nIn(nbOfChannels).stride(1, 1).padding(new int[] { 0, 0 })
						.nOut(nHidden[0]).activation(Activation.IDENTITY).build())
				.layer(new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX).kernelSize(2, 2).stride(2, 2)
						.build())
				.layer(new ConvolutionLayer.Builder(5, 5).stride(1, 1).nOut(nHidden[1]).activation(Activation.IDENTITY)
						.build())
				.layer(new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
						.kernelSize(2, 2).stride(2, 2).build())
				.layer(new DenseLayer.Builder().activation(Activation.RELU).nOut(nHidden[2]).build())
				.layer(new OutputLayer.Builder(
						org.nd4j.linalg.lossfunctions.LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD).nOut(nOut)
								.activation(Activation.SOFTMAX).build())
				.setInputType(InputType.convolutionalFlat(row, col, nbOfChannels)).build();
		MultiLayerNetwork model = new MultiLayerNetwork(conf);
		model.init();
		return model;
	}

	public static ConvolutionLayer convInit(String name, int in, int out, int[] kernel, int[] stride, int[] pad,
			double bias) {
		return new ConvolutionLayer.Builder(kernel, stride, pad).name(name).nIn(in).nOut(out).biasInit(bias).build();
	}

	public static ConvolutionLayer conv3x3(String name, int out, double bias) {
		return new ConvolutionLayer.Builder(new int[] { 3, 3 }, new int[] { 1, 1 }, new int[] { 1, 1 }).name(name)
				.nOut(out).biasInit(bias).build();
	}

	public static ConvolutionLayer conv5x5(String name, int out, int[] stride, int[] pad, double bias) {
		return new ConvolutionLayer.Builder(new int[] { 5, 5 }, stride, pad).name(name).nOut(out).biasInit(bias)
				.build();
	}

	public static SubsamplingLayer maxPool(String name, int[] kernel) {
		return new SubsamplingLayer.Builder(kernel, new int[] { 2, 2 }).name(name).build();
	}

	public static DenseLayer fullyConnected(String name, int out, double bias, double dropOut, Distribution dist) {
		return new DenseLayer.Builder().name(name).nOut(out).biasInit(bias).dropOut(dropOut).dist(dist).build();
	}

	public static MultiLayerNetworkParams lenetModel(int seed, int batchSize, int nbOfChannels, int nLabels, int h, int w) {
		/**
		 * Revisde Lenet Model approach developed by ramgo2 achieves slightly above
		 * random Reference:
		 * https://gist.github.com/ramgo2/833f12e92359a2da9e5c2fb6333351c5
		 **/
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed).l2(0.005)
				.activation(Activation.RELU).weightInit(WeightInit.XAVIER)
//            .updater(new Nadam(1e-4))s
				.updater(new AdaDelta()).list()
				.layer(0,
						convInit("cnn1", nbOfChannels, 50, new int[] { 5, 5 }, new int[] { 1, 1 }, new int[] { 0, 0 },
								0))
				.layer(1, maxPool("maxpool1", new int[] { 2, 2 }))
				.layer(2, conv5x5("cnn2", 100, new int[] { 5, 5 }, new int[] { 1, 1 }, 0))
				.layer(3, maxPool("maxool2", new int[] { 2, 2 })).layer(4, new DenseLayer.Builder().nOut(500).build())
				.layer(5,
						new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD).nOut(nLabels)
								.activation(Activation.SOFTMAX).build())
				.setInputType(InputType.convolutionalFlat(h, w, nbOfChannels)).build();
		
		MultiLayerNetwork net = new MultiLayerNetwork(conf);
		return new MultiLayerNetworkParams(net, seed, batchSize, nbOfChannels, net.getnLayers(), nLabels, h, w, NNType.CNN, "Lenet2");
	}

	public static MultiLayerNetworkParams lenetModel2(int seed, int batchSize, int nbOfChannels, int nLabels, int h, int w) {
		IUpdater updater = new AdaDelta();
		CacheMode cacheMode = CacheMode.NONE;
		WorkspaceMode workspaceMode = WorkspaceMode.ENABLED;
		ConvolutionLayer.AlgoMode cudnnAlgoMode = ConvolutionLayer.AlgoMode.PREFER_FASTEST;
		
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed)
                .activation(Activation.IDENTITY)
                .weightInit(WeightInit.XAVIER)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(updater)
                .cacheMode(cacheMode)
                .trainingWorkspaceMode(workspaceMode)
                .inferenceWorkspaceMode(workspaceMode)
                .cudnnAlgoMode(cudnnAlgoMode)
                .convolutionMode(ConvolutionMode.Same)
                .list()
                // block 1
                .layer(new ConvolutionLayer.Builder()
                        .name("cnn1")
                        .kernelSize(5, 5)
                        .stride(1, 1)
                        .nIn(nbOfChannels)
                        .nOut(20)
                        .activation(Activation.RELU)
                        .build())
                .layer(new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .name("maxpool1")
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build())
                // block 2
                .layer(new ConvolutionLayer.Builder()
                        .name("cnn2")
                        .kernelSize(5, 5)
                        .stride(1, 1)
                        .nOut(50)
                        .activation(Activation.RELU).build())
                .layer(new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .name("maxpool2")
                        .kernelSize(2, 2)
                        .stride(2, 2)
                        .build())
                // fully connected
                .layer(new DenseLayer.Builder()
                        .name("ffn1")
                        .activation(Activation.RELU)
                        .nOut(500)
                        .build())
                // output
                .layer(new OutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                        .name("output")
                        .nOut(nLabels)
                        .activation(Activation.SOFTMAX) // radial basis function required
                        .build())
                .setInputType(InputType.convolutionalFlat(h, w, nbOfChannels))
                .build();
		
		MultiLayerNetwork net = new MultiLayerNetwork(conf);
		return new MultiLayerNetworkParams(net, seed, batchSize, nbOfChannels, net.getnLayers(), nLabels, h, w, NNType.CNN, "Lenet2");
	}
	public static MultiLayerNetworkParams alexnetModel(int seed, int batchSize, int nbOfChannels, int nLabels, int h, int w) {
		h = 224;
		w = 224;
		/**
		 * AlexNet model interpretation based on the original paper ImageNet
		 * Classification with Deep Convolutional Neural Networks and the
		 * imagenetExample code referenced.
		 * http://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf
		 **/
		WorkspaceMode workspaceMode = WorkspaceMode.ENABLED;
		CacheMode cacheMode = CacheMode.NONE;
		ConvolutionLayer.AlgoMode cudnnAlgoMode = ConvolutionLayer.AlgoMode.PREFER_FASTEST;
		IUpdater updater = new Nesterovs(1e-2, 0.9);
		double nonZeroBias = 1;

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed)
                        .weightInit(new NormalDistribution(0.0, 0.01))
                        .activation(Activation.RELU)
                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                        .updater(updater)
                        .biasUpdater(new Nesterovs(2e-2, 0.9))
                        .convolutionMode(ConvolutionMode.Same)
                        .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer) // normalize to prevent vanishing or exploding gradients
                        .trainingWorkspaceMode(workspaceMode)
                        .inferenceWorkspaceMode(workspaceMode)
                        .cacheMode(cacheMode)
                        .l2(5 * 1e-4)
                        .miniBatch(false)
                        .list()
                .layer(0, new ConvolutionLayer.Builder(new int[]{11,11}, new int[]{4, 4})
                        .name("cnn1")
                        .cudnnAlgoMode(ConvolutionLayer.AlgoMode.PREFER_FASTEST)
                        .convolutionMode(ConvolutionMode.Truncate)
                        .nIn(nbOfChannels)
                        .nOut(96)
                        .build())
                .layer(1, new LocalResponseNormalization.Builder().build())
                .layer(2, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX)
                        .kernelSize(3,3)
                        .stride(2,2)
                        .padding(1,1)
                        .name("maxpool1")
                        .build())
                .layer(3, new ConvolutionLayer.Builder(new int[]{5,5}, new int[]{1,1}, new int[]{2,2})
                        .name("cnn2")
                        .cudnnAlgoMode(ConvolutionLayer.AlgoMode.PREFER_FASTEST)
                        .convolutionMode(ConvolutionMode.Truncate)
                        .nOut(256)
                        .biasInit(nonZeroBias)
                        .build())
                .layer(4, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX, new int[]{3, 3}, new int[]{2, 2})
                        .convolutionMode(ConvolutionMode.Truncate)
                        .name("maxpool2")
                        .build())
                .layer(5, new LocalResponseNormalization.Builder().build())
                .layer(6, new ConvolutionLayer.Builder()
                        .kernelSize(3,3)
                        .stride(1,1)
                        .convolutionMode(ConvolutionMode.Same)
                        .name("cnn3")
                        .cudnnAlgoMode(ConvolutionLayer.AlgoMode.PREFER_FASTEST)
                        .nOut(384)
                        .build())
                .layer(7, new ConvolutionLayer.Builder(new int[]{3,3}, new int[]{1,1})
                        .name("cnn4")
                        .cudnnAlgoMode(ConvolutionLayer.AlgoMode.PREFER_FASTEST)
                        .nOut(384)
                        .biasInit(nonZeroBias)
                        .build())
                .layer(8, new ConvolutionLayer.Builder(new int[]{3,3}, new int[]{1,1})
                        .name("cnn5")
                        .cudnnAlgoMode(ConvolutionLayer.AlgoMode.PREFER_FASTEST)
                        .nOut(256)
                        .biasInit(nonZeroBias)
                        .build())
                .layer(9, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX, new int[]{3,3}, new int[]{2,2})
                        .name("maxpool3")
                        .convolutionMode(ConvolutionMode.Truncate)
                        .build())
                .layer(10, new DenseLayer.Builder()
                        .name("ffn1")
                        .nIn(256*6*6)
                        .nOut(4096)
                        .weightInit(new NormalDistribution(0, 0.005))
                        .biasInit(nonZeroBias)
                        .build())
                .layer(11, new DenseLayer.Builder()
                        .name("ffn2")
                        .nOut(4096)
                        .weightInit(new NormalDistribution(0, 0.005))
                        .biasInit(nonZeroBias)
                        .dropOut(0.5)
                        .build())
                .layer(12, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .name("output")
                        .nOut(nLabels)
                        .activation(Activation.SOFTMAX)
                        .weightInit(new NormalDistribution(0, 0.005))
                        .biasInit(0.1)
                        .build())


                .setInputType(InputType.convolutional(h, w, nbOfChannels))
                .build();
        
		MultiLayerNetwork net = new MultiLayerNetwork(conf);
		return new MultiLayerNetworkParams(net, seed, batchSize, nbOfChannels, net.getnLayers(), nLabels, h, w, NNType.CNN, "AlexNet");
	}
	
	public static MultiLayerNetworkParams alexnetModel2(int seed, int batchSize, int nbOfChannels, int nLabels, int h, int w) {
		/**
		 * AlexNet model interpretation based on the original paper ImageNet
		 * Classification with Deep Convolutional Neural Networks and the
		 * imagenetExample code referenced.
		 * http://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf
		 **/

		double nonZeroBias = 1;
		double dropOut = 0.5;
		int out1 = 256;
		int out2 = 384;
		int out3 = 4096;
		out1 = 128;
		out2 = 192;
		out3 = 1024;

		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder().seed(seed)
				.weightInit(new NormalDistribution(0.0, 0.01)).activation(Activation.RELU).updater(new AdaDelta())
				.gradientNormalization(GradientNormalization.RenormalizeL2PerLayer) // normalize to prevent vanishing or
																					// exploding gradients
				.l2(5 * 1e-4).list()
				.layer(convInit("cnn1", nbOfChannels, 96, new int[] { 11, 11 }, new int[] { 4, 4 }, new int[] { 3, 3 },
						0))
				.layer(new LocalResponseNormalization.Builder().name("lrn1").build())
				.layer(maxPool("maxpool1", new int[] { 3, 3 }))
				.layer(conv5x5("cnn2", out1, new int[] { 1, 1 }, new int[] { 2, 2 }, nonZeroBias))
				.layer(new LocalResponseNormalization.Builder().name("lrn2").build())
				.layer(maxPool("maxpool2", new int[] { 3, 3 })).layer(conv3x3("cnn3", 384, 0))
				.layer(conv3x3("cnn4", out2, nonZeroBias)).layer(conv3x3("cnn5", 256, nonZeroBias))
				.layer(maxPool("maxpool3", new int[] { 3, 3 }))
				.layer(fullyConnected("ffn1", out3, nonZeroBias, dropOut, new GaussianDistribution(0, 0.005)))
				.layer(fullyConnected("ffn2", out3, nonZeroBias, dropOut, new GaussianDistribution(0, 0.005)))
				.layer(new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD).name("output")
						.nOut(nLabels).activation(Activation.SOFTMAX).build())
				.setInputType(InputType.convolutionalFlat(h, w, nbOfChannels)).build();

		MultiLayerNetwork net = new MultiLayerNetwork(conf);
		return new MultiLayerNetworkParams(net, seed, batchSize, nbOfChannels, net.getnLayers(), nLabels, h, w, NNType.CNN, "AlexNet2");
	}

	public static MultiLayerNetwork customModel() {
		/**
		 * Use this method to build your own custom model.
		 **/
		return null;
	}
}
