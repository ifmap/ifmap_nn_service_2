package models;

import org.deeplearning4j.common.resources.DL4JResources;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.graph.MergeVertex;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.zoo.ModelMetaData;
import org.deeplearning4j.zoo.PretrainedType;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.ZooType;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.AdaDelta;
import org.nd4j.linalg.learning.config.IUpdater;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.deeplearning4j.nn.graph.ComputationGraph;

public class SqueezeNet {

	private long seed = 1234;
	private int[] inputShape = new int[] { 3, 227, 227 };
	private int numClasses = 0;
	private WeightInit weightInit = WeightInit.RELU;
	private IUpdater updater = new AdaDelta();
	private CacheMode cacheMode = CacheMode.NONE;
	private WorkspaceMode workspaceMode = WorkspaceMode.ENABLED;
	private ConvolutionLayer.AlgoMode cudnnAlgoMode = ConvolutionLayer.AlgoMode.PREFER_FASTEST;

	public ComputationGraph squeezeNet(int nbLabels) {
		this.numClasses = nbLabels;
		ComputationGraph graph = conf();
		graph.init();

		return graph;
	}

	public ComputationGraph squeezeNet(int seed, int nbChannels, int nbLabels) {
		this.seed = seed;
		this.inputShape[0] = nbChannels;
		this.numClasses = nbLabels;
		ComputationGraph graph = conf();
		graph.init();

		return graph;
	}

	private ComputationGraph conf() {
		ComputationGraphConfiguration.GraphBuilder graph = graphBuilder();

		graph.addInputs("input").setInputTypes(InputType.convolutional(inputShape[2], inputShape[1], inputShape[0]));

		ComputationGraphConfiguration conf = graph.build();
		ComputationGraph model = new ComputationGraph(conf);
		model.init();

		return model;
	}

	public ComputationGraphConfiguration.GraphBuilder graphBuilder() {

		ComputationGraphConfiguration.GraphBuilder graph = new NeuralNetConfiguration.Builder().seed(seed)
				.optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT).updater(updater)
				.weightInit(weightInit).l2(5e-5).miniBatch(true).cacheMode(cacheMode)
				.trainingWorkspaceMode(workspaceMode).inferenceWorkspaceMode(workspaceMode)
				.convolutionMode(ConvolutionMode.Truncate).graphBuilder();

		graph
				// stem
				.addLayer("conv1",
						new ConvolutionLayer.Builder(3, 3).stride(2, 2).nOut(64).cudnnAlgoMode(cudnnAlgoMode).build(),
						"input")
				.addLayer("conv1_act", new ActivationLayer(Activation.RELU), "conv1")
				.addLayer("pool1", new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX).kernelSize(3, 3)
						.stride(2, 2).build(), "conv1_act");

		// fire modules
		fireModule(graph, 2, 16, 64, "pool1");
		fireModule(graph, 3, 16, 64, "fire2");
		graph.addLayer("pool3",
				new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX).kernelSize(3, 3).stride(2, 2).build(),
				"fire3");

		fireModule(graph, 4, 32, 128, "pool3");
		fireModule(graph, 5, 32, 128, "fire4");
		graph.addLayer("pool5",
				new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.MAX).kernelSize(3, 3).stride(2, 2).build(),
				"fire5");

		fireModule(graph, 6, 48, 192, "pool5");
		fireModule(graph, 7, 48, 192, "fire6");
		fireModule(graph, 8, 64, 256, "fire7");
		fireModule(graph, 9, 64, 256, "fire8");

		graph
				// output
				.addLayer("drop9", new DropoutLayer.Builder(0.5).build(), "fire9")
				.addLayer("conv10",
						new ConvolutionLayer.Builder(1, 1).nOut(numClasses).cudnnAlgoMode(cudnnAlgoMode).build(),
						"drop9")
				.addLayer("conv10_act", new ActivationLayer(Activation.RELU), "conv10")
				.addLayer("avg_pool", new GlobalPoolingLayer(PoolingType.AVG), "conv10_act")

				.addLayer("softmax", new ActivationLayer(Activation.SOFTMAX), "avg_pool")
				.addLayer("loss", new LossLayer.Builder(LossFunctions.LossFunction.MCXENT).build(), "softmax")

				.setOutputs("loss")

		;

		return graph;
	}

	private String fireModule(ComputationGraphConfiguration.GraphBuilder graphBuilder, int fireId, int squeeze,
			int expand, String input) {
		String prefix = "fire" + fireId;

		graphBuilder
				.addLayer(prefix + "_sq1x1",
						new ConvolutionLayer.Builder(1, 1).nOut(squeeze).cudnnAlgoMode(cudnnAlgoMode).build(), input)
				.addLayer(prefix + "_relu_sq1x1", new ActivationLayer(Activation.RELU), prefix + "_sq1x1")

				.addLayer(prefix + "_exp1x1",
						new ConvolutionLayer.Builder(1, 1).nOut(expand).cudnnAlgoMode(cudnnAlgoMode).build(),
						prefix + "_relu_sq1x1")
				.addLayer(prefix + "_relu_exp1x1", new ActivationLayer(Activation.RELU), prefix + "_exp1x1")

				.addLayer(prefix + "_exp3x3",
						new ConvolutionLayer.Builder(3, 3).nOut(expand).convolutionMode(ConvolutionMode.Same)
								.cudnnAlgoMode(cudnnAlgoMode).build(),
						prefix + "_relu_sq1x1")
				.addLayer(prefix + "_relu_exp3x3", new ActivationLayer(Activation.RELU), prefix + "_exp3x3")

				.addVertex(prefix, new MergeVertex(), prefix + "_relu_exp1x1", prefix + "_relu_exp3x3");

		return prefix;
	}
	
	public int[] getInputShape() {
		return this.inputShape;
	}
}
