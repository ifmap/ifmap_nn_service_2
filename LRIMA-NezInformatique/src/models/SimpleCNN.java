package models;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.zoo.ModelMetaData;
import org.deeplearning4j.zoo.PretrainedType;
import org.deeplearning4j.zoo.ZooModel;
import org.deeplearning4j.zoo.ZooType;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.AdaDelta;
import org.nd4j.linalg.learning.config.IUpdater;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import neural_network.MultiLayerNetworkParams;
import neural_network.NNType;

public class SimpleCNN {
	
	private long seed = 1234;
    private int[] inputShape = new int[] {3, 48, 48};
    private int numClasses = 0;
    private IUpdater updater = new AdaDelta();
    private CacheMode cacheMode = CacheMode.NONE;
    private WorkspaceMode workspaceMode = WorkspaceMode.ENABLED;
    private ConvolutionLayer.AlgoMode cudnnAlgoMode = ConvolutionLayer.AlgoMode.PREFER_FASTEST;
 
    public MultiLayerNetworkParams simpleCNN(int nbLabels, int batchSize) {
    	this.numClasses = nbLabels;
    	MultiLayerNetwork net = new MultiLayerNetwork(conf());
    	return new MultiLayerNetworkParams(net, (int) seed, batchSize, inputShape[0], net.getnLayers(), numClasses, inputShape[2], inputShape[1], NNType.CNN, "SimpleCNN");
    }
    
    public MultiLayerNetworkParams simpleCNN(int seed, int batchSize, int nbChannels, int nbLabels) {
    	this.seed = seed;
    	this.inputShape[0] = nbChannels;
    	this.numClasses = nbLabels;
    	MultiLayerNetwork net = new MultiLayerNetwork(conf());
    	return new MultiLayerNetworkParams(net, (int) seed, batchSize, inputShape[0], net.getnLayers(), numClasses, inputShape[2], inputShape[1], NNType.CNN, "SimpleCNN");
    }
    
    public MultiLayerConfiguration conf() {
        MultiLayerConfiguration conf =
                        new NeuralNetConfiguration.Builder().seed(seed)
                                        .activation(Activation.IDENTITY)
                                        .weightInit(WeightInit.RELU)
                                        .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                                        .updater(updater)
                                        .cacheMode(cacheMode)
                                        .trainingWorkspaceMode(workspaceMode)
                                        .inferenceWorkspaceMode(workspaceMode)
                                        .convolutionMode(ConvolutionMode.Same)
                                        .list()
                                        // block 1
                                        .layer(0, new ConvolutionLayer.Builder(new int[] {7, 7}).name("image_array")
                                                        .nIn(inputShape[0]).nOut(16).build())
                                        .layer(1, new BatchNormalization.Builder().build())
                                        .layer(2, new ConvolutionLayer.Builder(new int[] {7, 7}).nIn(16).nOut(16)
                                                        .build())
                                        .layer(3, new BatchNormalization.Builder().build())
                                        .layer(4, new ActivationLayer.Builder().activation(Activation.RELU).build())
                                        .layer(5, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.AVG,
                                                        new int[] {2, 2}).build())
                                        .layer(6, new DropoutLayer.Builder(0.5).build())

                                        // block 2
                                        .layer(7, new ConvolutionLayer.Builder(new int[] {5, 5}).nOut(32).build())
                                        .layer(8, new BatchNormalization.Builder().build())
                                        .layer(9, new ConvolutionLayer.Builder(new int[] {5, 5}).nOut(32).build())
                                        .layer(10, new BatchNormalization.Builder().build())
                                        .layer(11, new ActivationLayer.Builder().activation(Activation.RELU).build())
                                        .layer(12, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.AVG,
                                                        new int[] {2, 2}).build())
                                        .layer(13, new DropoutLayer.Builder(0.5).build())

                                        // block 3
                                        .layer(14, new ConvolutionLayer.Builder(new int[] {3, 3}).nOut(64).build())
                                        .layer(15, new BatchNormalization.Builder().build())
                                        .layer(16, new ConvolutionLayer.Builder(new int[] {3, 3}).nOut(64).build())
                                        .layer(17, new BatchNormalization.Builder().build())
                                        .layer(18, new ActivationLayer.Builder().activation(Activation.RELU).build())
                                        .layer(19, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.AVG,
                                                        new int[] {2, 2}).build())
                                        .layer(20, new DropoutLayer.Builder(0.5).build())

                                        // block 4
                                        .layer(21, new ConvolutionLayer.Builder(new int[] {3, 3}).nOut(128).build())
                                        .layer(22, new BatchNormalization.Builder().build())
                                        .layer(23, new ConvolutionLayer.Builder(new int[] {3, 3}).nOut(128).build())
                                        .layer(24, new BatchNormalization.Builder().build())
                                        .layer(25, new ActivationLayer.Builder().activation(Activation.RELU).build())
                                        .layer(26, new SubsamplingLayer.Builder(SubsamplingLayer.PoolingType.AVG,
                                                        new int[] {2, 2}).build())
                                        .layer(27, new DropoutLayer.Builder(0.5).build())


                                        // block 5
                                        .layer(28, new ConvolutionLayer.Builder(new int[] {3, 3}).nOut(256).build())
                                        .layer(29, new BatchNormalization.Builder().build())
                                        .layer(30, new ConvolutionLayer.Builder(new int[] {3, 3}).nOut(numClasses)
                                                        .build())
                                        .layer(31, new GlobalPoolingLayer.Builder(PoolingType.AVG).build())
                                        .layer(32, new ActivationLayer.Builder().activation(Activation.SOFTMAX).build())
                                        // fully connected
                                        .layer(33, new DenseLayer.Builder()
                                                .name("ffn1")
                                                .activation(Activation.RELU)
                                                .nOut(500)
                                                .build())
                                        // output
                                        .layer(34, new OutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                                                .name("output")
                                                .nOut(numClasses)
                                                .activation(Activation.SOFTMAX) // radial basis function required
                                                .build())

                                        .setInputType(InputType.convolutional(inputShape[2], inputShape[1],
                                                        inputShape[0]))
                                        .build();

        return conf;
    }

}
