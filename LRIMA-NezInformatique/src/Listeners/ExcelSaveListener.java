package Listeners;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import neural_network.ModelUtils;
import neural_network.MultiLayerNetworkParams;
import neural_network.NeuralNetworkParams;

public class ExcelSaveListener extends BaseTrainingListener {

	private String saveSheet;
	private DataSetIterator testData;
	private long startTime;
	private NeuralNetworkParams params;
	private int epochs = 0;
	private boolean showMatrix;
	private boolean useTransform;
	
	public ExcelSaveListener(String saveSheet, DataSetIterator testData, long startTime,
			NeuralNetworkParams params, boolean showMatrix, boolean useTransform) {
		this.saveSheet = saveSheet;
		this.testData = testData;
		this.startTime = startTime;
		this.params = params;
		this.epochs = 0;
		this.useTransform = useTransform;
		this.showMatrix = showMatrix;
	}

	@Override
	public void onEpochEnd(Model model) {
		System.out.println("Epoch:" + (++epochs));

		long trainTime = (System.currentTimeMillis() - startTime) / 1000;
		MultiLayerNetwork tempNet = null;
		ComputationGraph tempGraph = null;
		try {
			tempNet = (MultiLayerNetwork) model;
		} catch (Exception e) {
			try {
				tempGraph = (ComputationGraph) model;
			} catch (Exception e2) {
				System.err.println("Failed to parse lol");
				return;
			}	
		}
		
		try {
			if(tempNet != null)
				saveModelResults(ModelUtils.getEvaluation(tempNet, testData, showMatrix, true), trainTime, params.getBatchSize(), useTransform);
			else
				saveModelResults(ModelUtils.evaluateGraph(tempGraph, testData, false, true), trainTime, params.getBatchSize(), useTransform);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(Exception e) {
			System.err.println("Couldn't save model results");
		}
		
	}

	private void saveModelResults(Evaluation eval, long trainTime, int batchSize, boolean useTransform)
			throws FileNotFoundException, IOException {
		try {
			String[] headers = { "Number of channels", "Number of labels", "Number of layers",
					"Batch Size", "Epochs", "Accuracy", "Train Time", "Used Transform", "Model Type" };
			Object[] valuesToWrite = { params.getRows(), params.getnLabels(), params.getnLayers(),
					batchSize, epochs, eval.accuracy(), trainTime, useTransform, params.getNNConfName() };
			ModelUtils.saveModelResults(saveSheet, headers, valuesToWrite);
		} catch (EncryptedDocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

