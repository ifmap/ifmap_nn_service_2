package Listeners;


import java.io.File;
import java.io.IOException;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.optimize.api.BaseTrainingListener;
import org.deeplearning4j.optimize.listeners.CheckpointListener;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import neural_network.ModelUtils;

//Work with Computation Graph
public class ModelSaveListener extends BaseTrainingListener{
	
	private String modelName = "";
	private int nOut;
	
	private DataSetIterator testSet;
	
	private double bestAccuracy = 0;
	
	public ModelSaveListener(String modelName, int nOut, DataSetIterator testSet) {
		this.modelName = modelName;
		this.nOut = nOut;
		this.testSet = testSet;
	}
	
	@Override
	public void onEpochEnd(Model model) {
		if(model == null)
			return;
		
		MultiLayerNetwork net = null;
		ComputationGraph tempGraph = null;
		try {
			net = (MultiLayerNetwork) model;
		} catch (Exception e) {
			try {
				tempGraph = (ComputationGraph) model;
			} catch (Exception e2) {
				System.out.println("Couldn't evaluate and save model");
				return;
			}
		}
		
		Evaluation eval = null;
		if(net != null) {
			eval = ModelUtils.evaluateModel(net, testSet, false, false);
		} else {
			eval = ModelUtils.evaluateGraph(tempGraph, testSet, false, false);
		}
		
		double currentAcc = eval.accuracy();

		if(currentAcc > bestAccuracy) {
			if(removeOldModel())
				System.out.println("Removed old model, best accuracy was " + bestAccuracy + " and now is " + currentAcc);
			try {
				if(net != null)
					ModelUtils.saveModel(net, modelName, nOut, currentAcc);
				else
					ModelUtils.saveModel(tempGraph, modelName, nOut, currentAcc);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Couldn't save model");
				e.printStackTrace();
			}
			bestAccuracy = currentAcc;
			System.out.println("Saved new model!");
		}
	}
	
	private boolean removeOldModel() {
		File currentModel = null;
		File saveFile = new File(ModelUtils.MODEL_SAVE_PATH);
		
		if(saveFile == null)
			return false;
		
		for(File f : saveFile.listFiles()) {
			System.out.println(f.getPath());
			if(f.getPath().contains(modelName)) {
				currentModel = f;
				break;
			}
		}
		if(currentModel == null) {
			System.out.println("Couldn't find current model");
			return false;
		}	
		
		return currentModel.delete();
	}
}

