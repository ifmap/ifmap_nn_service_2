package Listeners;

import org.deeplearning4j.nn.api.Model;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;

/**
 * R�-�criture de la classe {@link ScoreIterationListener} qui permet
 * d'afficher le score du r�seau de neurones lors de l'entra�nement
 * dans la console
 * @author Thomas Tr�panier
 *
 */
public class CustomScoreIterationListener extends ScoreIterationListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private int printIterations = 10;

    /**
     * Constructeur de la classe. Permet de sp�cifier l'intervalle 
     * des it�rations entre les �critures dans la console
     * @param printIterations    frequency with which to print scores (i.e., every printIterations parameter updates)
     */
    public CustomScoreIterationListener(int printIterations) {
        this.printIterations = printIterations;
    }

    /** Default constructor printing every 10 iterations */
    public CustomScoreIterationListener() {}

    /**
     * M�thode appel�e � la fin d'une it�ration
     * @param model Le mod�le en cours d'entra�nement
     * @param iteration L'it�ration � laquelle l'entra�nement est rendu
     * @param epoch L'epoch � laquelle l'entra�nement est rendu
     */
    @Override
    public void iterationDone(Model model, int iteration, int epoch) {
        if (printIterations <= 0)
            printIterations = 1;
        if (iteration % printIterations == 0) {
            double score = model.score();
            System.out.println("Score at iteration " + iteration + " is " + score);
        }
    }

    @Override
    public String toString(){
        return "ScoreIterationListener(" + printIterations + ")";
    }
}
