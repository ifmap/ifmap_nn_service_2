Deeplearning4j OOM Exception Encountered for ComputationGraph
Timestamp:                              2019-12-11 09:56:24.335
Thread ID                               1
Thread Name                             main


Stack Trace:
java.lang.OutOfMemoryError: Cannot allocate new FloatPointer(1): totalBytes = 1024M, physicalBytes = 8294M
	at org.bytedeco.javacpp.FloatPointer.<init>(FloatPointer.java:76)
	at org.nd4j.linalg.api.buffer.BaseDataBuffer.<init>(BaseDataBuffer.java:710)
	at org.nd4j.linalg.api.buffer.FloatBuffer.<init>(FloatBuffer.java:54)
	at org.nd4j.linalg.api.buffer.factory.DefaultDataBufferFactory.create(DefaultDataBufferFactory.java:290)
	at org.nd4j.linalg.factory.Nd4j.getDataBuffer(Nd4j.java:1467)
	at org.nd4j.linalg.factory.Nd4j.createTypedBuffer(Nd4j.java:1486)
	at org.nd4j.linalg.cpu.nativecpu.CpuNDArrayFactory.create(CpuNDArrayFactory.java:345)
	at org.nd4j.linalg.factory.Nd4j.scalar(Nd4j.java:5019)
	at org.nd4j.linalg.api.ops.BaseScalarOp.<init>(BaseScalarOp.java:64)
	at org.nd4j.linalg.api.ops.impl.scalar.RectifiedLinear.<init>(RectifiedLinear.java:47)
	at org.nd4j.linalg.api.ops.impl.scalar.RectifiedLinear.<init>(RectifiedLinear.java:55)
	at org.nd4j.linalg.activations.impl.ActivationReLU.getActivation(ActivationReLU.java:37)
	at org.deeplearning4j.nn.layers.mkldnn.MKLDNNConvHelper.activate(MKLDNNConvHelper.java:164)
	at org.deeplearning4j.nn.layers.convolution.ConvolutionLayer.activate(ConvolutionLayer.java:459)
	at org.deeplearning4j.nn.graph.vertex.impl.LayerVertex.doForward(LayerVertex.java:111)
	at org.deeplearning4j.nn.graph.ComputationGraph.ffToLayerActivationsInWS(ComputationGraph.java:2135)
	at org.deeplearning4j.nn.graph.ComputationGraph.computeGradientAndScore(ComputationGraph.java:1372)
	at org.deeplearning4j.nn.graph.ComputationGraph.computeGradientAndScore(ComputationGraph.java:1341)
	at org.deeplearning4j.optimize.solvers.BaseOptimizer.gradientAndScore(BaseOptimizer.java:170)
	at org.deeplearning4j.optimize.solvers.StochasticGradientDescent.optimize(StochasticGradientDescent.java:63)
	at org.deeplearning4j.optimize.Solver.optimize(Solver.java:52)
	at org.deeplearning4j.nn.graph.ComputationGraph.fitHelper(ComputationGraph.java:1165)
	at org.deeplearning4j.nn.graph.ComputationGraph.fit(ComputationGraph.java:1115)
	at org.deeplearning4j.nn.graph.ComputationGraph.fit(ComputationGraph.java:1082)
	at org.deeplearning4j.nn.graph.ComputationGraph.fit(ComputationGraph.java:1018)
	at org.deeplearning4j.nn.graph.ComputationGraph.fit(ComputationGraph.java:1006)
	at neural_network.ModelUtils.trainModel(ModelUtils.java:63)
	at neural_network.RecognitionCNN.runCC(RecognitionCNN.java:151)
	at neural_network.RecognitionCNN.main(RecognitionCNN.java:107)
Caused by: java.lang.OutOfMemoryError: Physical memory usage is too high: physicalBytes (8294M) > maxPhysicalBytes (8096M)
	at org.bytedeco.javacpp.Pointer.deallocator(Pointer.java:589)
	at org.bytedeco.javacpp.Pointer.init(Pointer.java:125)
	at org.bytedeco.javacpp.FloatPointer.allocateArray(Native Method)
	at org.bytedeco.javacpp.FloatPointer.<init>(FloatPointer.java:68)
	... 28 more


========== Memory Information ==========
----- Version Information -----
Deeplearning4j Version                  1.0.0-beta5
Deeplearning4j CUDA                     <not present>

----- System Information -----
Operating System                        Microsoft Windows 10
CPU                                     Intel(R) Core(TM) i7-8700 CPU @ 3.20GHz
CPU Cores - Physical                    6
CPU Cores - Logical                     12
Total System Memory                      15,81 GiB (16973897728)

----- ND4J Environment Information -----
Data Type                               FLOAT
blas.vendor                             MKL
os                                      Windows 10
backend                                 CPU

----- Memory Configuration -----
JVM Memory: XMX                           3,95 GiB (4244635648)
JVM Memory: current                     120,00 MiB (125829120)
JavaCPP Memory: Max Bytes                 3,95 GiB (4244635648)
JavaCPP Memory: Max Physical              7,91 GiB (8489271296)
JavaCPP Memory: Current Bytes             1,00 GiB (1074786024)
JavaCPP Memory: Current Physical          7,92 GiB (8508325888)
Periodic GC Enabled                     false

----- Workspace Information -----
Workspaces: # for current thread        2
Current thread workspaces:
  Name                      State       Size                          # Cycles            
  WS_LAYER_WORKING_MEM      CLOSED           ,00 B                    9                   
  WS_ALL_LAYERS_ACT         CLOSED        2,43 GiB (2613166080)       1                   
Workspaces total size                     2,43 GiB (2613166080)

----- Network Information -----
Network # Parameters                    134272835
Parameter Memory                        512,21 MiB (537091340)
Parameter Gradients Memory              512,21 MiB (537091340)
Updater                                 <not initialized>
Params + Gradient + Updater Memory      512,21 MiB (537091340)
Iteration Count                         0
Epoch Count                             0
Backprop Type                           Standard
Workspace Mode: Training                ENABLED
Workspace Mode: Inference               ENABLED
Number of Layers                        21
Layer Counts
  ConvolutionLayer                        13
  DenseLayer                              2
  OutputLayer                             1
  SubsamplingLayer                        5
Layer Parameter Breakdown
  Idx Name                 Layer Type           Layer # Parameters   Layer Parameter Memory
  1   0                    ConvolutionLayer     1792                   7,00 KiB (7168)   
  2   1                    ConvolutionLayer     36928                144,25 KiB (147712) 
  3   2                    SubsamplingLayer     0                         ,00 B          
  4   3                    ConvolutionLayer     73856                288,50 KiB (295424) 
  5   4                    ConvolutionLayer     147584               576,50 KiB (590336) 
  6   5                    SubsamplingLayer     0                         ,00 B          
  7   6                    ConvolutionLayer     295168                 1,13 MiB (1180672)
  8   7                    ConvolutionLayer     590080                 2,25 MiB (2360320)
  9   8                    ConvolutionLayer     590080                 2,25 MiB (2360320)
  10  9                    SubsamplingLayer     0                         ,00 B          
  11  10                   ConvolutionLayer     1180160                4,50 MiB (4720640)
  12  11                   ConvolutionLayer     2359808                9,00 MiB (9439232)
  13  12                   ConvolutionLayer     2359808                9,00 MiB (9439232)
  14  13                   SubsamplingLayer     0                         ,00 B          
  15  14                   ConvolutionLayer     2359808                9,00 MiB (9439232)
  16  15                   ConvolutionLayer     2359808                9,00 MiB (9439232)
  17  16                   ConvolutionLayer     2359808                9,00 MiB (9439232)
  18  17                   SubsamplingLayer     0                         ,00 B          
  19  18                   DenseLayer           102764544            392,02 MiB (411058176)
  20  19                   DenseLayer           16781312              64,02 MiB (67125248)
  21  20                   OutputLayer          12291                 48,01 KiB (49164)  

----- Layer Helpers - Memory Use -----
Total Helper Count                      18
Helper Count w/ Memory                  0
Total Helper Persistent Memory Use           ,00 B

----- Network Activations: Inferred Activation Shapes -----
Current Minibatch Size                  50
Current Input Shape (Input 0)           [50, 3, 224, 224]
Idx Name                 Layer Type           Activations Type                           Activations Shape    # Elements   Memory      
0   in                   InputVertex          InputTypeConvolutional(h=224,w=224,c=3)    [50, 3, 224, 224]    7526400       28,71 MiB (30105600)
1   0                    ConvolutionLayer     InputTypeConvolutional(h=224,w=224,c=64)   [50, 64, 224, 224]   160563200    612,50 MiB (642252800)
2   1                    ConvolutionLayer     InputTypeConvolutional(h=224,w=224,c=64)   [50, 64, 224, 224]   160563200    612,50 MiB (642252800)
3   2                    SubsamplingLayer     InputTypeConvolutional(h=112,w=112,c=64)   [50, 64, 112, 112]   40140800     153,12 MiB (160563200)
4   3                    ConvolutionLayer     InputTypeConvolutional(h=112,w=112,c=128)  [50, 128, 112, 112]  80281600     306,25 MiB (321126400)
5   4                    ConvolutionLayer     InputTypeConvolutional(h=112,w=112,c=128)  [50, 128, 112, 112]  80281600     306,25 MiB (321126400)
6   5                    SubsamplingLayer     InputTypeConvolutional(h=56,w=56,c=128)    [50, 128, 56, 56]    20070400      76,56 MiB (80281600)
7   6                    ConvolutionLayer     InputTypeConvolutional(h=56,w=56,c=256)    [50, 256, 56, 56]    40140800     153,12 MiB (160563200)
8   7                    ConvolutionLayer     InputTypeConvolutional(h=56,w=56,c=256)    [50, 256, 56, 56]    40140800     153,12 MiB (160563200)
9   8                    ConvolutionLayer     InputTypeConvolutional(h=56,w=56,c=256)    [50, 256, 56, 56]    40140800     153,12 MiB (160563200)
10  9                    SubsamplingLayer     InputTypeConvolutional(h=28,w=28,c=256)    [50, 256, 28, 28]    10035200      38,28 MiB (40140800)
11  10                   ConvolutionLayer     InputTypeConvolutional(h=28,w=28,c=512)    [50, 512, 28, 28]    20070400      76,56 MiB (80281600)
12  11                   ConvolutionLayer     InputTypeConvolutional(h=28,w=28,c=512)    [50, 512, 28, 28]    20070400      76,56 MiB (80281600)
13  12                   ConvolutionLayer     InputTypeConvolutional(h=28,w=28,c=512)    [50, 512, 28, 28]    20070400      76,56 MiB (80281600)
14  13                   SubsamplingLayer     InputTypeConvolutional(h=14,w=14,c=512)    [50, 512, 14, 14]    5017600       19,14 MiB (20070400)
15  14                   ConvolutionLayer     InputTypeConvolutional(h=14,w=14,c=512)    [50, 512, 14, 14]    5017600       19,14 MiB (20070400)
16  15                   ConvolutionLayer     InputTypeConvolutional(h=14,w=14,c=512)    [50, 512, 14, 14]    5017600       19,14 MiB (20070400)
17  16                   ConvolutionLayer     InputTypeConvolutional(h=14,w=14,c=512)    [50, 512, 14, 14]    5017600       19,14 MiB (20070400)
18  17                   SubsamplingLayer     InputTypeConvolutional(h=7,w=7,c=512)      [50, 512, 7, 7]      1254400        4,79 MiB (5017600)
19  18                   DenseLayer           InputTypeFeedForward(4096)                 [50, 4096]           204800       800,00 KiB (819200)
20  19                   DenseLayer           InputTypeFeedForward(4096)                 [50, 4096]           204800       800,00 KiB (819200)
21  20                   OutputLayer          InputTypeFeedForward(3)                    [50, 3]              150            600,00 B  
Total Activations Memory                  2,84 GiB (3047322200)
Total Activation Gradient Memory          2,84 GiB (3047321600)

----- Network Training Listeners -----
Number of Listeners                     3
Listener 0                              org.deeplearning4j.optimize.listeners.CollectScoresIterationListener@6c4f9535
Listener 1                              Listeners.ExcelSaveListener@5bd1ceca
Listener 2                              Listeners.ModelSaveListener@30c31dd7
